import React, { Component } from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap/dist/js/bootstrap.min.js';
import Sidebar from './Components/Sidebar/Sidebar';

import GroupPage from './Pages/GroupPage/GroupPage';
import MasterPage from './Pages/Masterpage/MasterPage';
import DataComponents from './Pages/Masterpage/DataComponents';
import DataSeason from './Pages/Masterpage/DataSeason';
import DataContactInfo from './Pages/Masterpage/DataContactInfo';
import CustomerCataloge from './Pages/CustomerCataloge/CustomerCataloge';
import Kaizen from './Pages/Kaizen/Kaizen';
import ListData from './Components/List_Data/ListDataTable';
import Toast from './Components/Toast/Toast';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div className="wrapper">
          <Sidebar />
  
          <div className="main">
            <Route path="/" exact component={GroupPage}/>
            <Route path="/customer-cataloge" exact component={MasterPage}/>
            <Route path="/customer-cataloge/components" exact component={DataComponents}/> 
            <Route path="/customer-cataloge/season" exact component={DataSeason}/> 
            <Route path="/customer-cataloge/contacts" exact component={DataContactInfo}/> 
            <Route path="/customer-cataloge/export" component={CustomerCataloge} />
            <Route path="/kaizen" exact component={Kaizen}/>
            <Route path="/list-data" exact component={ListData}/>

            <Toast />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
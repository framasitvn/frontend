import React from 'react';

const Toast = (props) => {
    return(
        <div role="alert" aria-live="assertive" aria-atomic="true" className="toast" data-autohide="true" data-delay="3500" data-animation="true">
          <div className="toast-header bg-success text-light">
            <i className="fas fa-bell mr-2"></i>
            <strong className="mr-auto">Notification</strong>
            {/* <small>now</small> */}
            <button type="button" className="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
              <span aria-hidden="true" className="text-light">&times;</span>
            </button>
          </div>
          <div className="toast-body">
            New record is added successfully!!!
          </div>
        </div>
    )
}

export default Toast;
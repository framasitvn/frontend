import React, { Component } from 'react';
import PageNav from '../PageNav/PageNav';

class PageHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageNav: props.pageNav,
            headline: props.headline,
            backgroundImage: props.backgroundImage
        };
    }

    menuToggleHandler = () => {
        let wrapper = document.querySelector(".wrapper");
        let sidebar = document.querySelector(".sidebar");
        wrapper.classList.toggle("toggled");
        sidebar.classList.toggle("minimize");
    }
    
    render () {
        return (
            <div className="page-header" style={{backgroundImage: `url(${this.state.backgroundImage})`}}>
    
                <button className="btn btn-light" id="menu-toggle" onClick={this.menuToggleHandler} data-html2canvas-ignore="true"><i className="fa fa-bars"
                        aria-hidden="true"></i></button>
                
                { this.state.pageNav === "true" ? <PageNav /> : "" }
    
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 ml-auto mr-auto text-center text-light">
                            <h1 className="title">{this.props.headline}</h1>
                        </div>
                    </div>
                </div>
            </div>
        )
    }   
}

export default PageHeader
import React from 'react';
import { Link } from 'react-router-dom';

const PageNav = () => {
    return (
        <div>
            {/* <!-- NAV --> */}
            <ul className="page-nav main-nav nav">
                <li className="nav-item">
                    <Link className="nav-link active dropdown-toggle" to="/customer-cataloge" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Database</Link>

                    <div className="dropdown-menu">
                        <Link className="dropdown-item datatable-item" to="/customer-cataloge">Main</Link>
                        <Link className="dropdown-item datatable-item" to="/customer-cataloge/season">Season</Link>
                        <Link className="dropdown-item datatable-item" to="/customer-cataloge/components">Components</Link>
                        <Link className="dropdown-item datatable-item" to="/customer-cataloge/contacts">Contact Information</Link>
                    </div>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/customer-cataloge/export">Export</Link>
                </li>
            </ul>
        </div>
    )
}

export default PageNav
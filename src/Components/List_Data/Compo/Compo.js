import React from "react";
import axios from 'axios';
import $ from 'jquery';

export default class components extends React.Component {
  state = {
    components:[]
  }

  componentDidMount() {
      this.loadData();
      this.onSubmitComponent();
      this.modifyComponent();
  };

  //LoadData
  loadData(){
    axios.post("http://192.168.9.54:8088/api/component/all")
    .then(res => {
      const components = res.data.result;
      this.setState({ components });
      require( 'datatables.net-responsive-bs4' );
      $("#componentTable").DataTable({
        retrieve: true,
        "scrollY": "65vh",
        "scrollCollapse": true,
        "paging": false,
        responsive: true,
        scroller: {
            loadingIndicator: true
        }
      })
      this.modifyClick();
    })
    .catch(error => console.log(error));

  };

  //Add Item
  onSubmitComponent() {
    const addBtn = document.querySelector("#add-compo");

    addBtn.onclick = () => {
      var comCode = $("#comCode").val().toString();
      var comName = $("#comName").val().toString();

      if (comCode === "") {
      $(".alert-warning").html("<strong>Component Code</strong> must be not empty.");
      $(".alert-warning").show();
      $("#comCode").focus();
      return false;
      } else {
      if (comName === "") {
          $(".alert-warning").html("<strong>Component Name</strong> must be not empty.");
          $(".alert-warning").show();
          $("#comName").focus();
          return false;
      }
      else {
          axios({
          method: 'post',
          url: "http://192.168.9.54:8088/api/component/add",
          data: {
              meta_data: {
              code: comCode,
              name: comName
              },
              username: "dong"
          },
          headers: {
              'Content-Type': 'application/json'
          }
          }).then(res => {
          console.log(res);
          console.log(res.data);

          setTimeout(function(){$('#modalComponent').modal('hide')},0);  
          $(".alert-success").html("<strong>Success!</strong> Has successfully add new record !");
          $(".alert-success").show();
          $(".alert-warning").hide();

          this.componentDidMount();
            
          });
      }
      }
    };
  };
  
  //Modify Item
  modifyComponent () {
    const modifyBtn = document.querySelector("#modify-compo");

    modifyBtn.onclick = () => {
      var comID = parseInt($("#comID").val());
      var comCode = $("#comCode").val();
      var comName = $("#comName").val();
      var comStatus = $("#comStatus").val() === "0" ? false : true;
    
      if (comCode === "") {
      $(".alert-warning").html("<strong>Component Code</strong> must be not empty.");
      $(".alert-warning").show();
      $("#comCode").focus();
      return false;
      } else {
      if (comName === "") {
          $(".alert-warning").html("<strong>Component Name</strong> must be not empty.");
          $(".alert-warning").show();
          $("#comName").focus();
          return false;
      }
      else {
          axios({
          method: 'post',
          url: "http://192.168.9.54:8088/api/component/modify",
          data: {
              meta_data: {
              "code": comCode,
              "id": comID,
              "is_active": comStatus,
              "name": comName
              },
              "username": "dong"
          },
          headers: {
              'Content-Type': 'application/json'
          }
          }).then(res => {
          console.log(res);
          console.log(res.data);

          setTimeout(function(){$('#modalComponent').modal('hide')},0);
          $(".alert-success").html("<strong>Success!</strong> Has successfully modify record ID = "+comID+" !");
          $(".alert-success").show();
          $(".alert-warning").hide();

          this.componentDidMount();
          });
      }
      }
    };
  };

  //Event click
  addClick() {
    //Set value
    $("#comCode").val("");
    $("#comName").val("");

    //Button hide
    $("#add-compo").show();
    $("#modify-compo").hide();

    //Input hide
    $(".comStatus").hide();

    //Message hide
    // $(".alert-success").hide();
     $(".alert-warning").hide();
  };

  modifyClick() {
    //Set value
    let btns = document.querySelectorAll(".comModify");
    btns.forEach(el => {
      el.onclick = () => {
        var id = el.getAttribute("data-com-id");
        var code = el.getAttribute("data-com-code");
        var name = el.getAttribute("data-com-name");
  
        $(".comModify").attr({"data-toggle":"modal", "data-target":"#modalComponent"});
        $("#comID").val(id);
        $("#comCode").val(code);
        $("#comName").val(name);
      }
    });

    //Button hide
    $("#modify-compo").show();
    $("#add-compo").hide();

    //Input hide
    $(".comStatus").show();

    //Message hide
    // $(".alert-success").hide();
     $(".alert-warning").hide();
  };

  showComponent() {
    $(".table-component").toggle();
  };


  render() {
    return (
        <div className="wrap-component col">
            <div className="row">
              <div className="col">
                
                {/* Alert-Success */}
                  <div className="alert alert-success text-center alert-dismissible hide" role="alert">
                  <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                
                {/* Button */}
                <div className="row mb-2">
                    <div className="col">
                        <button className="btn btn-success" data-toggle="modal" data-target="#modalComponent" onClick={this.addClick}>
                          <i className="fa fa-plus mr-1"></i>Add Record
                        </button>
                    </div>
                </div>                

                {/* Data Table */}
                <div className="row">
                  <div className="col">
                      <table id="componentTable" className="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          { this.state.components.map(components => 
                            <tr key={components.Id}>
                              <td className='valueComID' name={components.Id}>{components.Id}</td>
                              <td className='valueComCode' name={components.Code}>{components.Code}</td>
                              <td className='valueComName' name={components.Name}>{components.Name}</td>
                              <td>{components.IsActive ? "Active" : "Deactive"}</td>
                              <td>
                                <span className="comModify" name={components.Id} onClick={this.modifyClick}
                                      data-com-id={components.Id}
                                      data-com-code={components.Code}
                                      data-com-name={components.Name}>
                                  <i className="fa fa-edit"></i>
                                </span>
                                </td>
                            </tr> 
                          )}
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
        </div>
    );
}
}
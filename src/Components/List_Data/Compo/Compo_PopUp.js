import React from "react";

export default class comPopUp extends React.Component {
            
      
    render(){
        return(
            <div className="modal fade" id="modalComponent" tabIndex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                    <div className="modal-dialog modal-notify modal-info" role="document">
                        {/* <!--Content--> */}
                        <div className="modal-content text-center">
                        {/* <!--Header--> */}
                        <div className="modal-header d-flex justify-content-center">
                            <h2 className="heading">Components</h2>
                        </div>

                        {/* <!--Body--> */}
                        <div className="modal-body text-left">

                        <form method="POST GET PUT DELETE" autoComplete="off">
                      <div className="grey-text">

                        <div className="form-group hide comID">
                          <label>Component ID</label>
                          <input className="form-control" id="comID" />
                        </div>

                        <div className="form-group">
                          <label>Component Code</label>
                          <input className="form-control" id="comCode" />
                        </div>

                        <div className="form-group">
                          <label>Component Name</label>
                          <input className="form-control" id="comName" />
                        </div>

                        <div className="form-group comStatus">
                          <label>Status</label>
                          <select className="form-control" id="comStatus">
                            <option value="1">Active</option>
                            <option value="0">Deactive</option>
                          </select>
                        </div>

                        <div className="form-group hide">
                          <label>Update By</label>
                          <input className="form-control" id="comUpdate" />
                        </div>

                        <div className="form-group hide">
                          <label>Create By</label>
                          <input className="form-control" id="comCreate" />
                        </div>
                      </div>

                      <div className="text-center">
                        <div className="alert alert-warning alert-dismissible hide">
                          <div className="close" data-dismiss="alert" aria-label="close">&times;</div>
                        </div>
                      </div>
                      <div className="text-center">
                      </div>
                    </form>
                        </div>

                        {/* <!--Footer--> */}
                        <div className="modal-footer flex-center">
                          <button className="btn btn-primary" id="add-compo">
                            Add <i className=" fa fa-paper-plane ml-1"></i>
                          </button>

                          <button className="btn btn-primary" id="modify-compo">
                            Save <i className=" fa fa-paper-plane ml-1"></i>
                          </button>

                          <button className="cancel-compo btn btn-danger" data-dismiss="modal">
                            Cancel <i className=" fa fa-times ml-1"></i>
                          </button>
                          </div>
                        </div>
                        {/* <!--/.Content--> */}
                    </div>
                  </div>
        );
    }
}
import React from "react";

export default class seasonPopUp extends React.Component {
            
      
    render(){
        return(
            <div className="modal fade" id="modalSeason" tabIndex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div className="modal-dialog modal-notify modal-info" role="document">
                {/* <!--Content--> */}
                <div className="modal-content text-center">
                {/* <!--Header--> */}
                <div className="modal-header d-flex justify-content-center">
                    <h2 className="heading">Seasons</h2>
                </div>

                {/* <!--Body--> */}
                <div className="modal-body text-left">
                    <form method="POST GET PUT DELETE" autoComplete="off">
                        <div className="row grey-text">
                            <div className="col">
                                    <div className="form-group hide ssID hide">
                                        <label>Season ID</label>
                                        <input className="form-control" id="ssID"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Season Code</label>
                                        <input className="form-control" id="ssCode"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Season Name</label>
                                        <input className="form-control" id="ssName"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Start Date</label>
                                        <input type="date" className="form-control" id="start"/>
                                    </div>
                                    <div className="form-group">
                                        <label>End Date</label>
                                        <input type="date" className="form-control" id="end"/>
                                    </div>
                                    <div className="form-group ssStatus">
                                        <label>Status</label>
                                        <select className="form-control" id="ssStatus">
                                            <option value="1">Active</option>
                                            <option value="0">Deactive</option>
                                        </select>
                                    </div>
                                    <div className="form-group hide">
                                        <label>Update By</label>
                                        <input className="form-control" id="ssUpdate"/>
                                    </div>
                                    <div className="form-group hide">
                                        <label>Create By</label>
                                        <input className="form-control" id="ssCreate"/>
                                    </div>
                                </div>
                        </div>

                        <div className="text-center">
                            <div className="alert alert-warning alert-dismissible hide">
                                <div className="close" data-dismiss="alert" aria-label="close">&times;</div>
                            </div>
                        </div>
                    </form>
                </div>

                {/* <!--Footer--> */}
                <div className="modal-footer flex-center">
                    <button className="btn btn-primary" id="add-season">
                        Add<i className="fa fa-paper-plane"></i>
                    </button>

                    <button className="btn btn-primary" id="modify-season">
                        Save <i className="fa fa-paper-plane ml-1"></i>
                    </button>

                    <button className="cancel-season btn btn-danger" data-dismiss="modal">
                        Cancel <i className="fa fa-times ml-1"></i>
                    </button>
                </div>
                </div>
                {/* <!--/.Content--> */}
            </div>
            </div>
        )
    }
    }
import React from "react";
import axios from 'axios';
import $ from 'jquery';

export default class seasons extends React.Component {
    state = {
        seasons:[]
      }

componentDidMount() {
  this.loadData();
  this.onSubmitSeason();
  this.modifySeason();
};

//LoadData
loadData(){
  axios.post('http://192.168.9.54:8088/api/season/all')
  .then(res => {
    const seasons = res.data.result;
    this.setState({ seasons });

    require( 'datatables.net-bs4' );
      $("#seasonTable").DataTable({
        retrieve: true,
        "scrollY": "65vh",
        "scrollCollapse": true,
        "paging": false,
        responsive: true,
        scroller: {
            loadingIndicator: true
        }
      });
      this.modifyClick();
  })
  .catch(error => console.log(error));
};

//add
onSubmitSeason() {
  const addBtn = document.querySelector("#add-season");

  addBtn.onclick = () => {
  var code = $("#ssCode").val();
  var end = $("#end").val();
  var name = $("#ssName").val();
  var start = $("#start").val();

  if(code ===""){
    $(".alert-warning").html("<strong>Code</strong> must be not empty !");
    $(".alert-warning").show();
    $("#ssCode").focus();
  }else{
    if(name ===""){
      $(".alert-warning").html("<strong>Name</strong> must be not empty !");
      $(".alert-warning").show();
      $("#ssName").focus();
  }else{
    if(start ===""){
      $(".alert-warning").html("<strong>Start Date</strong> must be not empty !");
      $(".alert-warning").show();
      $("#start").focus();
  }else{
    if(end ===""){
      $(".alert-warning").html("<strong>End Date</strong> must be not empty !");
      $(".alert-warning").show();
      $("#end").focus();
  }else{
    axios({
      method: 'post',
      url: "http://192.168.9.54:8088/api/season/add",
      data: {
        meta_data: {
          code: code,
          end_date: end,
          name: name,
          start_date: start
        },
        username: "dong"
      },
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => {
      console.log(res);
      console.log(res.data);

      setTimeout(function(){$('#modalSeason').modal('hide')},0);

      $(".alert-success").html("<strong>Success!</strong> Has successfully add new record.");
      $(".alert-success").show();
      $(".alert-warning").hide();

      this.componentDidMount();
    });
  }}}}
}
  };

//modify
modifySeason() {
  const modifyBtn = document.querySelector("#modify-season");
  modifyBtn.onclick = () => {
    var updateSSID = parseInt($("#ssID").val());
    var code = $("#ssCode").val();
    var end = $("#end").val();
    var name = $("#ssName").val();
    var start = $("#start").val();
    var status = $("#ssStatus").val();
  
    if(code ===""){
      $(".alert-warning").html("<strong>Code</strong> must be not empty !");
      $(".alert-warning").show();
      $("#ssCode").focus();
    }else{
      if(name ===""){
        $(".alert-warning").html("<strong>Name</strong> must be not empty !");
        $(".alert-warning").show();
        $("#ssName").focus();
    }else{
      if(start ===""){
        $(".alert-warning").html("<strong>Start Date</strong> must be not empty !");
        $(".alert-warning").show();
        $("#start").focus();
    }else{
      if(end ===""){
        $(".alert-warning").html("<strong>End Date</strong> must be not empty !");
        $(".alert-warning").show();
        $("#end").focus();
    }else{
        axios({
          method: 'post',
          url: "http://192.168.9.54:8088/api/season/modify",
          data: {
            meta_data: {
              code: code,
              end_date: end,
              id: updateSSID,
              is_active: status,
              name: name,
              start_date: start,
            },
            username: "dong"
          },
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(res => {
          console.log(res);
          console.log(res.data);

          setTimeout(function(){$('#modalSeason').modal('hide')},0);
    
          $(".alert-success").html("<strong>Success!</strong> Has successfully modify record ID = "+updateSSID+" !");
          $(".alert-success").show();
          $(".alert-warning").hide();

          this.componentDidMount();
        });
          }
        }
      }
    }
  };
};

//event click
addClick(){
    //Set value
    $("#ssCode").val("");
    $("#ssName").val("");
    $("#start").val("");
    $("#end").val("");

    //Button hide
    $("#modify-season").hide();
    $("#add-season").show();
    
    //Input hide
    $(".ssStatus").hide();

    //Message hide
    //$(".alert-success").hide();
    $(".alert-warning").hide();

  };

modifyClick(){
    //Set value
    let btns = document.querySelectorAll(".ssModify");
    btns.forEach(el => {
      el.onclick = () => {
        var id = el.getAttribute("data-ss-id");
        var code = el.getAttribute("data-ss-code");
        var name = el.getAttribute("data-ss-name");
        var start = el.getAttribute("data-ss-start");
        var end = el.getAttribute("data-ss-end");
  
        $(".ssModify").attr({"data-toggle":"modal", "data-target":"#modalSeason"});
        $("#comID").val(id);
        $("#comCode").val(code);
        $("#comName").val(name);

        $("#ssID").val(id);
        $("#ssCode").val(code);
        $("#ssName").val(name);
        $("#start").val(start);
        $("#end").val(end);
        }
    });
    
    //Input show
    //$(".ssID").show();
    $(".ssStatus").show();

    //Button hide
    $("#add-season").hide();
    $("#modify-season").show();

    //Message hide
    //$(".alert-success").hide();
    $(".alert-warning").hide();
};

showSeason(){
  $(".table-season").toggle();
};


  render() {
    return (
        <div className="wrap-season col">
        <div className="row">
        <div className="col">
            
            {/* Alert-Success */}
            <div className="alert alert-success text-center alert-dismissible hide" role="alert">
                <strong>Added Successfully !</strong>
              <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>

            {/* Button */}
            <div className="row mb-2">
                <div className="col">
                    <button className="btn btn-success" data-toggle="modal" data-target="#modalSeason" onClick={this.addClick}>
                      <i className="fa fa-plus mr-1"></i>Add Record
                    </button>
                </div>
            </div>

            {/* Form-pop-up */}
            

            {/* Data Table */}
            <div className="row">
                <div className="col">
                <table id="seasonTable" className="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Code</th>
                      <th>Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    { this.state.seasons.map(seasons => 
                      <tr key={seasons.Id}>
                        <td>{seasons.Id}</td>
                        <td>{seasons.Code}</td>
                        <td>{seasons.Name}</td>
                        <td>{seasons.StartDate}</td>
                        <td>{seasons.EndDate}</td>
                        <td>{seasons.IsActive ? "Active" : "Deactive"}</td>
                        <td>
                          <span className="ssModify" name={seasons.Id} onClick={this.modifyClick}
                                data-ss-id={seasons.Id}
                                data-ss-code={seasons.Code}
                                data-ss-name={seasons.Name}
                                data-ss-start={seasons.StartDate}
                                data-ss-end={seasons.EndDate}>
                            <i className="fa fa-edit"></i>
                            </span>
                        </td>
                      </tr> 
                    )}
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
    </div>  
    );
}
}
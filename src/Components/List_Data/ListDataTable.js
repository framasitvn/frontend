import React, { Component } from "react";
import $ from 'jquery';

import Components from './Compo/Compo';
import Contacts from './Contact_Infor/Contact';
import Seasons from './Season/Season';


class NavbarPage extends Component {

showComponent(){
    $(".wrap-component").toggle();
    $(".wrap-contact").hide();
    $(".wrap-season").hide();
  };

  showContact(){
    $(".wrap-contact").toggle();
    $(".wrap-component").hide();
    $(".wrap-season").hide();
  };

  showSeason(){
    $(".wrap-season").toggle();
    $(".wrap-component").hide();
    $(".wrap-contact").hide();
  };


render() {
  return (
      <div>
        
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <span className="navbar-brand">
    Navbar
  </span>
  <button
    className="navbar-toggler"
    type="button"
    data-toggle="collapse"
    data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent"
    aria-expanded="false"
    aria-label="Toggle navigation"
  >
    <span className="navbar-toggler-icon" />
  </button>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <div className="nav-link">
          Home <span className="sr-only">(current)</span>
        </div>
      </li>
      <li className="nav-item">
        <div className="nav-link">
          Link
        </div>
      </li>
      <li className="nav-item dropdown">
        <div className="nav-link dropdown-toggle" id="navbarDropdown"
          role="button"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false" >
          Dropdown
        </div>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <div className="dropdown-item" onClick={this.showComponent}>
            Components
          </div>
          <div className="dropdown-item" onClick={this.showContact}>
            Contacts Infor
          </div>
          <div className="dropdown-item" onClick={this.showSeason}>
            Seasons
          </div>
        </div>
      </li>
    </ul>
  </div>
</nav>
        
        <Components/>
        <Contacts/>
        <Seasons/>
      </div>
    );
  }
}

export default NavbarPage;
import React from "react";
import Factory from './Factory';

export default class contactPopUp extends React.Component {
            
      
    render(){
        return(
            <div className="modal fade" id="modalContact" tabIndex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                  <div className="modal-dialog modal-notify modal-info" role="document">
                      {/* <!--Content--> */}
                      <div className="modal-content text-center">
                        {/* <!--Header--> */}
                        <div className="modal-header d-flex justify-content-center">
                            <h2 className="heading">Contacts Info</h2>
                        </div>

                        {/* <!--Body--> */}
                        <div className="modal-body">
                          <form method="POST GET PUT DELETE" autoComplete="off">

                              {/* factory-select */}
                            <div className="row grey-text">
                              <div className="col">
                                <Factory/>
                              </div>
                            </div>
                              {/* group-input */}
                            <div className="row grey-text">
                              <div className="col">
                                  <div className="form-group conID hide">
                                      <label>Contact ID</label>
                                      <input className="form-control" id="conID"/>
                                  </div>
                                  
                                  <div className="form-group">
                                      <label>Title</label>
                                      <input className="form-control" id="title"/>
                                  </div>
                                  <div className="form-group">
                                      <label>First Name</label>
                                      <input className="form-control" id="fName"/>
                                  </div>
                                  <div className="form-group">
                                      <label>Last Name</label>
                                      <input className="form-control" id="lName"/>
                                  </div>
                              </div>
                              <div className="col">
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input className="form-control" id="email"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Phone No</label>
                                        <input type="number" className="form-control" id="phone"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Phone No 2</label>
                                        <input type="number" className="form-control" id="phone2"/>
                                    </div>
                                </div>
                            </div>
                              {/* status-select */}
                            <div className="row">
                              <div className="col">
                                <div className="form-group conStatus">
                                  <label>Status</label>
                                  <select className="form-control" id="conStatus">
                                      <option value="1">Active</option>
                                      <option value="0">Deactive</option>
                                  </select>
                                </div>
                              </div>
                            </div>

                              {/* message */}
                            <div className="text-center">
                              <div className="alert alert-warning alert-dismissible hide">
                                <div className="close" data-dismiss="alert" aria-label="close">&times;</div>
                              </div>
                            </div>
                          </form>
                        </div>

                        {/* <!--Footer--> */}
                        <div className="modal-footer flex-center">
                        <button className="btn btn-primary" id="add-contact">
                          Add<i className="fa fa-paper-plane"></i>
                        </button>

                        <button className="btn btn-primary" id="modify-contact">
                          Save <i className="fa fa-paper-plane ml-1"></i>
                        </button>

                        <button className="cancel-contact btn btn-danger" data-dismiss="modal">
                          Cancel <i className="fa fa-times ml-1"></i>
                      </button>
                      </div>
                      </div>
                      {/* <!--/.Content--> */}
                  </div>
                </div>
        )
    }
    }
import React from 'react';
import axios from 'axios';


export default class factory extends React.Component {
  state = {
    factory: []
  }

  componentDidMount() {
    axios.post(`http://192.168.9.54:8088/api/factory/all`)
      .then(res => {
        const data = res.data.result;
        this.setState({ factory : data});
      })
      .catch(error => console.log(error));
  }


  render() {
    return (
      <div className="form-group">
          <label>Factory</label>
          <select className="form-control" id="facID">
            { this.state.factory.map(factory => <option value={factory.Id} key={factory.Id}>{factory.Name}</option>)}
          </select>
      </div>
    )
  }
}
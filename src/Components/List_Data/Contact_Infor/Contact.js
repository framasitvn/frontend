import React from "react";
import axios from 'axios';
import $ from 'jquery';


export default class contacts extends React.Component {
  state = {
    contacts:[]
  }

  componentDidMount() {
    this.loadData();
    this.onSubmitContact();
    this.modifyContact();
  };

  //Load Data
  loadData(){
    axios.post("http://192.168.9.54:8088/api/contactinfo/all")
    .then(res => {
      const contacts = res.data.result;
      
      this.setState({contacts});

      require( 'datatables.net-responsive-bs4' )
      $("#contactTable").DataTable({
        retrieve: true,
        "scrollY": "65vh",
        "scrollCollapse": true,
        "paging": false,
        responsive: true,
        scroller: {
            loadingIndicator: true
        }
      });

      this.modifyClick();
    })
    .catch(error => console.log(error));
  };

  //Add Item
  onSubmitContact(){
    const addBtn = document.querySelector("#add-contact");

    addBtn.onclick = () => {

      var email = $("#email").val();
      var facID = $("#facID").val();
      var fName = $("#fName").val();
      var lName = $("#lName").val();
      var phone = $("#phone").val();
      var phone2 = $("#phone2").val();
      var title = $("#title").val();

      if(title === ""){
        $(".alert-warning").html("<strong>Title</strong> must be not empty.");
        $(".alert-warning").show();
        $("#title").focus();
        return false;
    }else{
      if(fName === ""){
        $(".alert-warning").html("<strong>First Name</strong> must be not empty.");
        $(".alert-warning").show();
        $("#fName").focus();
        return false;
    }else{
      if(lName === ""){
        $(".alert-warning").html("<strong>Last Name</strong> must be not empty.");
        $(".alert-warning").show();
        $("#lName").focus();
        return false;
    }else{
      if(email === ""){
        $(".alert-warning").html("<strong>Email</strong> must be formatted exactly as: <strong>example@email.com</strong>");
        $(".alert-warning").show();
        $("#email").focus();
        return false;
    }else{
      if(phone === ""){
        $(".alert-warning").html("<strong>Phone No</strong> must be not empty.");
        $(".alert-warning").show();
        $("#phone").focus();
        return false;
    }else{
      if(phone2 === ""){
        $(".alert-warning").html("<strong>Phone No 2</strong> must be not empty.");
        $(".alert-warning").show();
        $("#phone2").focus();
        return false;
    }else{
      axios({
        method: 'post',
        url: "http://192.168.9.54:8088/api/contactinfo/add",
        data: {
          meta_data: {
            email: email,
            factory_id: facID,
            first_name: fName,
            last_name: lName,
            phone_no: phone,
            phone_no_2: phone2,
            title: title
          },
          username: "dong"
        },
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(res => {
        console.log(res);
        console.log(res.data);
        
        setTimeout(function(){$('#modalContact').modal('hide')},0);

        $(".alert-success").html("<strong>Success!</strong> Has successfully add new record.");
        $(".alert-success").show();
        $(".alert-warning").hide();

        this.componentDidMount();
      });
    }}}}}}
    }
  };

  //Modify Item
  modifyContact() {
    const modifyBtn = document.querySelector("#modify-contact");

    modifyBtn.onclick = () => {
    var conID = parseInt($("#conID").val());
    var email = $("#email").val();
    var facID = parseInt($("#facID").val());
    var fName = $("#fName").val();
    var lName = $("#lName").val();
    var phone = $("#phone").val();
    var phone2 = $("#phone2").val();
    var title = $("#title").val();
    var status = $("#conStatus").val();

    if(title === ""){
        $(".alert-warning").html("<strong>Title</strong> must be not empty.");
        $(".alert-warning").show();
        $("#title").focus();
        return false;
    }else{
      if(fName === ""){
        $(".alert-warning").html("<strong>First Name</strong> must be not empty.");
        $(".alert-warning").show();
        $("#fName").focus();
        return false;
    }else{
      if(lName === ""){
        $(".alert-warning").html("<strong>Last Name</strong> must be not empty.");
        $(".alert-warning").show();
        $("#lName").focus();
        return false;
    }else{
      if(email === ""){
        $(".alert-warning").html("<strong>Email</strong> must be formatted exactly as: <strong>example@email.com</strong>");
        $(".alert-warning").show();
        $("#email").focus();
        return false;
    }else{
      if(phone === ""){
        $(".alert-warning").html("<strong>Phone No</strong> must be not empty.");
        $(".alert-warning").show();
        $("#phone").focus();
        return false;
    }else{
      if(phone2 === ""){
        $(".alert-warning").html("<strong>Phone No 2</strong> must be not empty.");
        $(".alert-warning").show();
        $("#phone2").focus();
        return false;
    }else{ 
        axios({
          method: 'post',
          url: "http://192.168.9.54:8088/api/contactinfo/modify",
          data: {
            meta_data: {
            email: email,
            factory_id: facID,
            first_name: fName,
            id: conID,
            is_active: status,
            last_name: lName,
            phone_no: phone,
            phone_no_2: phone2,
            title: title
            },
            username: "dong"
          },
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(res => {
          console.log(res);
          console.log(res.data);
          
          setTimeout(function(){$('#modalContact').modal('hide')},0);

          $(".alert-success").html("<strong>Success!</strong> Has successfully modify record ID = "+conID+" !");
          $(".alert-success").show();
          $(".alert-warning").hide();

          this.componentDidMount();
        });
  };}}}}}}
  };

  //Event click
  addClick() {
    //Set value
    $("#email").val("");
    $("#fName").val("");
    $("#lName").val("");
    $("#phone").val("");
    $("#phone2").val("");
    $("#title").val("");

    //Button hide
    $("#add-contact").show();
    $("#modify-contact").hide();

    //Input hide
    $(".conID").hide();
    $(".conStatus").hide();

    //Message hide
    //$(".alert-success").hide();
    $(".alert-warning").hide();

    //Block select factory
    $("#facID option").attr('disabled', 'disabled');
    $("#facID option[value='61']").removeAttr('disabled');
    $("#facID option[value='61']").attr('selected', 'selected');
  };


  modifyClick() {
    //Set value
    let btns = document.querySelectorAll(".conModify");
    btns.forEach(el => {
      el.onclick = () => {
        var id = el.getAttribute("data-con-id");
        var title = el.getAttribute("data-con-title");
        var fName = el.getAttribute("data-con-fname");
        var lName = el.getAttribute("data-con-lname");
        var email = el.getAttribute("data-con-email");
        var phone = el.getAttribute("data-con-phone");
        var phone2 = el.getAttribute("data-con-phone2");
  
        $(".conModify").attr({"data-toggle":"modal", "data-target":"#modalContact"});
        $("#conID").val(id);
        $("#title").val(title);
        $("#fName").val(fName);
        $("#lName").val(lName);
        $("#email").val(email);
        $("#phone").val(phone);
        $("#phone2").val(phone2);

        //Button hide
        $("#modify-contact").show();
        $("#add-contact").hide();

        //Input hide
        //$(".conID").show();
        $(".conStatus").show();

        //Message hide
        //$(".alert-success").hide();
        $(".alert-warning").hide();

        //Block select factory
        $("#facID option").attr('disabled', 'disabled');
        $("#facID option[value='61']").removeAttr('disabled');
        $("#facID option[value='61']").attr('selected', 'selected');
      }
    });
  };

  showContact() {
    $(".table-contact").toggle();
  };


  render() {
    return (
        <div className="wrap-contact col">
            <div className="row">
            <div className="col">
                
                {/* Alert-Success */}
                <div className="alert alert-success text-center alert-dismissible hide" role="alert">
                    <strong>Added Successfully !</strong>
                  <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                
                {/* Button */}
                <div className="row mb-2">
                    <div className="col">
                        <button className="btn btn-success" data-toggle="modal" data-target="#modalContact" onClick={this.addClick}>
                          <i className="fa fa-plus mr-1"></i>Add Record
                        </button>
                    </div>
                </div>

                {/* Form-pop-up */}
                

                {/* Data Table */}
                <div className="row">
                    <div className="col">
                    <table id="contactTable" className="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Factory Id</th>
                            <th>Title</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone No</th>
                            <th>Phone No 2</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          { this.state.contacts.map(contacts => 
                            <tr key={contacts.Id}>
                              <td className='valueConID' name={contacts.Id}>{contacts.Id}</td>
                              <td className='valueFacID' name={contacts.FactoryId}>{contacts.FactoryId}</td>
                              <td className='valueConTitle' name={contacts.Title}>{contacts.Title}</td>
                              <td className='valueConFName' name={contacts.FirstName}>{contacts.FirstName}</td>
                              <td className='valueConLName' name={contacts.LastName}>{contacts.LastName}</td>
                              <td className='valueConEmail' name={contacts.Email}>{contacts.Email}</td>
                              <td className='valueConPhone' name={contacts.PhoneNo}>{contacts.PhoneNo}</td>
                              <td className='valueConPhone2' name={contacts.PhoneNo2}>{contacts.PhoneNo2}</td>
                              <td>{contacts.IsActive ? "Active" : "Deactive"}</td>
                              <td>
                                  <span data-con-id={contacts.Id} 
                                          data-fac-id={contacts.FactoryId} 
                                          data-con-title={contacts.Title} 
                                          data-con-fname={contacts.FirstName} 
                                          data-con-lname={contacts.LastName} 
                                          data-con-email={contacts.Email} 
                                          data-con-phone={contacts.PhoneNo} 
                                          data-con-phone2={contacts.PhoneNo2} 
                                          className="conModify">
                                    <i className="fa fa-edit"></i>
                                  </span>
                              </td>
                            </tr> 
                          )}
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
}
}
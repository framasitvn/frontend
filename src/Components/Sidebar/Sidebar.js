import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false
        };
    }

    calHeightSidebar = () => {
        var logoHeight = document.querySelector(".logo").clientHeight;
        var searchBoxHeight = document.querySelector(".search-group").clientHeight;
        var mainNav = document.querySelector(".main-nav");

        mainNav.style.height = `calc(100vh - (${logoHeight}px + ${searchBoxHeight}px))`;
    }

    collapedSubNavHandler = (e) => {
        const currentState = this.state.active;
        this.setState({ active: !currentState });

        let el = e.currentTarget;
        el.classList.toggle("expanded");

        let subNav = el.childNodes[1];
        let subNavHeight = el.childNodes[1].childNodes[0].clientHeight;

        if (currentState !== true) {
            subNav.style.height = `${subNavHeight}px`;
        } else {
            subNav.style.height = `0px`;
        }
    }

    activeRouteNav = () => {
        const navItems = document.querySelectorAll(".nav-item .nav-link");

        navItems.forEach(el => {
            el.onclick = () => {
                navItems.forEach(element => {
                    element.classList.remove("active");
                })
                el.classList.add("active");
            }
        })
    }

    componentDidMount() {
        this.calHeightSidebar();
        this.activeRouteNav();
    }

    render () { 
        return (
            <div className="sidebar">
                <div className="sidebar-container">
                    <div className="logo"><Link to="/"><img src={require('../../Assets/images/logo.png')} className="img-fluid" alt="framas-logo" /></Link></div>

                    <div className="search-group input-group mt-4 mb-4">
                        <div className="input-group-prepend">
                            <div className="input-group-text"><i className="fa fa-search" aria-hidden="true"></i></div>
                        </div>
                        <input type="text" className="form-control" id="search" placeholder="Search" />
                    </div>

                    {/* NAV */}
                    <ul className="main-nav nav flex-column">
                        <li className="nav-item">
                            <Link className="nav-link" to="/"><i className="fa fa-home mr-2" aria-hidden="true"></i><p className="m-0">Home</p></Link>
                        </li>
                        <li className="nav-item nav-has-sub" onClick={this.collapedSubNavHandler.bind(this)}>
                            <Link className="nav-link" to="#"><i className="fa fa-asterisk mr-2" aria-hidden="true"></i><p className="m-0">Project</p> <b
                                    className="caret"></b></Link>

                            <div className="sub-nav">
                                <ul className="nav links__nav">
                                    <li className="sub-nav-item">
                                        <Link className="sub-nav-link" to="/kaizen">Kaizen</Link>
                                    </li>
                                    <li className="sub-nav-item">
                                        <Link className="sub-nav-link" to="/customer-cataloge">Customer Cataloge</Link>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="#"><i className="fa fa-tasks mr-2" aria-hidden="true"></i><p className="m-0">My Tasks</p> <span
                                    className="badge badge-danger ml-2">5</span></Link>
                        </li>
                        <li className="nav-item nav-has-sub" onClick={this.collapedSubNavHandler.bind(this)}>
                            <Link className="nav-link" to="#"><i className="fa fa-table mr-2" aria-hidden="true"></i><p className="m-0">My Reports</p> <b
                                    className="caret"></b></Link>

                            <div className="sub-nav">
                                <ul className="nav links__nav">
                                    <li className="sub-nav-item">
                                        <Link className="sub-nav-link" to="#">APT</Link>
                                    </li>
                                    <li className="sub-nav-item">
                                        <a href="/holiday" className="sub-nav-link">Cost Accounting</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-item nav-has-sub" onClick={this.collapedSubNavHandler.bind(this)}>
                            <Link className="nav-link" to="#"><i className="fa fa-calendar mr-2" aria-hidden="true"></i><p className="m-0">My Calendars</p>
                                <b className="caret"></b></Link>

                            <div className="sub-nav">
                                <ul className="nav links__nav">
                                    <li className="sub-nav-item">
                                        <Link className="sub-nav-link" to="#">Holiday <span
                                                className="badge badge-danger ml-2">4</span></Link>
                                    </li>
                                    <li className="sub-nav-item">
                                        <Link className="sub-nav-link" to="#">Annual Leave</Link>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-item nav-has-sub" onClick={this.collapedSubNavHandler.bind(this)}>
                            <Link className="nav-link" to="#"><i className="fa fa-compass mr-2" aria-hidden="true"></i><p className="m-0">My OKRs</p> <b
                                    className="caret"></b></Link>

                            <div className="sub-nav">
                                <ul className="nav links__nav">
                                    <li className="sub-nav-item">
                                        <Link className="sub-nav-link" to="#">Framas Hanoi</Link>
                                    </li>
                                    <li className="sub-nav-item">
                                        <Link className="sub-nav-link" to="#">Finance & Controlling</Link>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Sidebar;
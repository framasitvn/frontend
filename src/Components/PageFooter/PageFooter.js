import React from 'react';
import { Link } from 'react-router-dom';

const PageFooter = () => {
    return (
        <div>
            <footer>
                <div className="container-fluid d-flex flex-wrap justify-content-between">
                    <nav>
                        <ul className="footer-menu p-0">
                            <li><Link className="router-link-exact-active active" to="/">Dashboards</Link></li>
                        </ul>
                    </nav>
                    <div className="copyright">
                        © Framas 2020. All right reserved.
                    </div>
                </div>
            </footer>
        </div>
    )
}

export default PageFooter;
import React from 'react';
import Form from 'react-bootstrap/Form';
import axiosConfig from '../../../Store/axiosConfig.js';

export default class ProductList extends React.Component {
  state = {
    product: []
  }

  componentDidMount() {
    axiosConfig.post(`/api/productgroup/all`)
      .then(res => {
        const product = res.data.result;
        this.setState({ product });
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <Form.Control as="select">        
        <option value="0">All</option>
        { this.state.product.map(product => <option key={`${product.Name}-${product.Id}`} value={product.Id}>{product.Name}</option>)}
      </Form.Control>
    )
  }
}
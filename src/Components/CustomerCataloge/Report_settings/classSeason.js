import React from 'react';
import Form from 'react-bootstrap/Form';
import axiosConfig from '../../../Store/axiosConfig.js';

export default class SeasonList extends React.Component {
  state = {
    season: []
  }

  componentDidMount() {
    axiosConfig.post(`/api/season/all`)
      .then(res => {
        const season = res.data.result;
        this.setState({ season });
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <Form.Control as="select">        
        <option value="0">All</option>
        { this.state.season.map(season => <option key={`${season.Name}-${season.Id}`} value={season.Id}>{season.Name}</option>)}
      </Form.Control>
    )
  }
}
import React from 'react';
import Form from 'react-bootstrap/Form';
import axiosConfig from '../../../Store/axiosConfig.js';

export default class FactoryList extends React.Component {
  state = {
    factory: []
  }

  componentDidMount() {
    axiosConfig.post(`/api/factory/all`)
      .then(res => {
        const factory = res.data.result;
        this.setState({ factory });
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <Form.Control as="select">
        <option value="0">All</option>
        { this.state.factory.map(factory => <option key={`${factory.Name}-${factory.Id}`} value={factory.Id}>{factory.Name}</option>)}
      </Form.Control>
    )
  }
}
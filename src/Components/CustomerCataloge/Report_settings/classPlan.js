import React from 'react';
import Form from 'react-bootstrap/Form';
import axiosConfig from '../../../Store/axiosConfig.js';

export default class PlanList extends React.Component {
  state = {
    plant: []
  }

  componentDidMount() {
    axiosConfig.post(`/api/project/all`)
      .then(res => {
        const plant = res.data.result;
        this.setState({ plant });
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <Form.Control as="select">
          <option value="0">All</option>
          { this.state.plant.map(plant => <option key={`${plant.Name}-${plant.Id}`} value={plant.Id}>{plant.Name}</option>)}
      </Form.Control>
    )
  }
}
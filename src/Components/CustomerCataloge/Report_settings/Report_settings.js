import React, { Component } from 'react';
import { connect } from 'react-redux';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';

import Plans from '../Report_settings/classPlan';
import Brand from '../Report_settings/classBrand';
import Factory from '../Report_settings/classFactory';
import Season from '../Report_settings/classSeason';
import ProductGroup from '../Report_settings/classProduct_Group';
import Category from '../Report_settings/classCategory';

import PDFCusommerCataloge from '../PDF_export/PDF_export';
import PPTCustomerCataloge from "../PPT_export/PPT_export";

import { PDFDownloadLink } from '@react-pdf/renderer';
import html2canvas from 'html2canvas';

class Report_setting extends Component {
    // constructor(props) {
    //     super(props);
    // }

    state = {
        headline: require('../../../Assets/images/framas-logo.jpg'),
        report_item_1: require('../../../Assets/images/framas-logo.jpg'),
        report_item_2: require('../../../Assets/images/framas-logo.jpg'),
        footer: require('../../../Assets/images/framas-logo.jpg'),
    }

    html2canvasHandler = () => {
        let html_img = []
        
        const send_IMG_DATA = (canvas) => {
            const imgData = canvas.toDataURL('image/png');
            return imgData;
        }

        const html2canvasConfig = (srcElementClassName) => {
            let el = document.querySelector(`.${srcElementClassName}`);
            
            return html2canvas(el, {
                allowTaint: true,
                useCORS: true,
                imageTimeout: 0,
                scrollX: 0,
                scrollY: -window.scrollY,
                scale: 1
            });
        }

        html2canvasConfig("headline")
            .then((canvas) => {
                html_img.push(send_IMG_DATA(canvas))

                html2canvasConfig("footer")
                .then((canvas) => {
                    html_img.push(send_IMG_DATA(canvas))

                    html2canvasConfig("report-item-1")
                        .then((canvas) => {
                            html_img.push(send_IMG_DATA(canvas))

                            html2canvasConfig("report-item-2")
                                .then((canvas) => {
                                    html_img.push(send_IMG_DATA(canvas))

                                    this.setState({
                                        headline: html_img[0],
                                        footer: html_img[1],
                                        report_item_1: html_img[2],
                                        report_item_2: html_img[3]
                                    })
                                }).catch((err) => {console.log(err)});

                        }).catch((err) => {console.log(err)});

                }).catch((err) => {console.log(err)});

            }).catch((err) => {console.log(err)});
    };

    exportPDFHandler = () => {
        document.querySelector('.btn-export-pdf').addEventListener('click', function (event) {
            if (this.classList.contains('isDisabled')) {
                event.preventDefault();
            } else {
                console.log('EXPORT!!!')
            }
        });
    };

    refreshSettingHandler = (e) => {
        e.preventDefault();

        let headline = document.querySelector('.headline');
        let report_settings = document.querySelector('.report-settings');
        let report = document.querySelector('.report');
        let btn_config = document.querySelector('.btn-config');
        let btn_export_pdf = document.querySelector('.btn-export-pdf button');
        let link_export_pdf = document.querySelector('.btn-export-pdf');
        let footer_bg = document.querySelector('.footer-bg');
        // let report_list_data = document.querySelector('.report-list-data');

        report_settings.classList.remove('fixed-top', 'show-report');
        report.classList.add('d-none');
        report.classList.remove('mt-5');
        btn_config.classList.add('d-none');
        headline.classList.remove('show-report');
        btn_export_pdf.disabled = true;
        link_export_pdf.classList.add('isDisabled');
        footer_bg.classList.add('d-none');
        // report_list_data.classList.remove('hide');

        this.exportPDFHandler();
    }

    applyReportHandler = (e) => {
        e.preventDefault();

        let headline = document.querySelector('.headline');
        let report_settings = document.querySelector('.report-settings');
        let report_specs = document.querySelector('.report-specs');
        let report = document.querySelector('.report');
        let btn_config = document.querySelector('.btn-config');
        let link_export_pdf = document.querySelector('.btn-export-pdf');
        let btn_export_pdf = document.querySelector('.btn-export-pdf button');
        let footer_bg = document.querySelector('.footer-bg');
        // let report_list_data = document.querySelector('.report-list-data');

        report_settings.classList.add("fixed-top", "show-report");
        report.classList.remove('d-none');
        btn_config.classList.remove('d-none');
        report_specs.classList.add('invisible');
        report.classList.add('mt-5');
        headline.classList.add('show-report');
        btn_export_pdf.disabled = false;
        link_export_pdf.classList.remove('isDisabled');
        footer_bg.classList.remove('d-none');
        // report_list_data.classList.add('hide');

        this.exportPDFHandler();
        this.html2canvasHandler();
    }

    btnConfigHandler = (e) => {
        e.preventDefault();

        let report_specs = document.querySelector('.report-specs');
        report_specs.classList.toggle('invisible');
    }

    render() {
        return(
            <div className="report-settings" data-html2canvas-ignore="true">
                <Container>
                    <Row>
                        <Col xs={12} lg={8} className="report-filter">
                            <Card>
                                <Card.Header>Report Settings</Card.Header>
                                <Card.Body>
                                    <Form>
                                        <Form.Group as={Row} controlId="filter_plant">
                                            <Form.Label column sm="2" className="pr-0">
                                            Plant
                                            </Form.Label>
                                            <Col sm="10">
                                                <Plans/>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} controlId="filter_brand">
                                            <Form.Label column sm="2" className="pr-0">
                                            Brand
                                            </Form.Label>
                                            <Col sm="10">
                                                <Brand/>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} controlId="filter_factory">
                                            <Form.Label column sm="2" className="pr-0">
                                            Factory
                                            </Form.Label>
                                            <Col sm="10">                                           
                                                <Factory/>                                     
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} controlId="filter_season">
                                            <Form.Label column sm="2" className="pr-0">
                                            Season
                                            </Form.Label>
                                            <Col sm="10">
                                                <Season/>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} controlId="filter_product_group">
                                            <Form.Label column sm="2" className="pr-0">
                                            Product Group
                                            </Form.Label>
                                            <Col sm="10">
                                                <ProductGroup/>
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} controlId="filter_category">
                                            <Form.Label column sm="2" className="pr-0">
                                            Category
                                            </Form.Label>
                                            <Col sm="10">
                                                <Category/>
                                            </Col>
                                        </Form.Group>
                                    </Form>

                                    <Button className="btn-apply" variant="dark" block onClick={this.applyReportHandler}><i className="fa fa-download mr-1" aria-hidden="true"></i> APPLY</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={12} lg={4} className="report-specs mt-3 mt-lg-0">
                            <Card>
                                <Card.Header>Report Specs</Card.Header>
                                <Card.Body>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, veritatis. Et amet blanditiis, harum esse voluptatem veniam corrupti excepturi!</p>
                                    
                                    <Table striped bordered hover size="sm">
                                        <thead>
                                            <tr>
                                            <th>#</th>
                                            <th colSpan="2">Report</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td>1</td>
                                            <td>Items</td>
                                            <td>21</td>
                                            </tr>
                                            <tr>
                                            <td>2</td>
                                            <td>Pages</td>
                                            <td>23</td>
                                            </tr>
                                            <tr>
                                            <td>3</td>
                                            <td>Row</td>
                                            <td>550</td>
                                            </tr>
                                        </tbody>
                                    </Table>

                                    <Container className="text-light mt-4 p-0">
                                        <Row>
                                            <Col xs={12} lg={6}>
                                                {/* <Button variant="danger" block disabled className="btn-export-pdf"><i className="fas fa-file-pdf mr-1"></i> Export PDF</Button> */}
                                                
                                                <PDFDownloadLink 
                                                    document={
                                                        <PDFCusommerCataloge 
                                                            headline={this.state.headline}
                                                            report_item_1={this.state.report_item_1}
                                                            report_item_2={this.state.report_item_2}
                                                            footer={this.state.footer}
                                                        />} 
                                                    fileName="framas-customer-cataloge.pdf"
                                                    className="btn-export-pdf text-decoration-none text-light isDisabled"
                                                    onLoad={this.exportPDFHandler}>
                                                    {({ blob, url, loading, error }) => (loading ? 'Loading document...' : <Button variant="danger" block disabled><i className="fa fa-file-pdf-o mr-1" aria-hidden="true"></i> Export PDF</Button>)}
                                                </PDFDownloadLink>
                                            </Col>
                                            <Col xs={12} lg={6}>    
                                                <PPTCustomerCataloge
                                                    headline={this.state.headline}
                                                    report_item_1={this.state.report_item_1}
                                                    report_item_2={this.state.report_item_2}
                                                    footer={this.state.footer} />
                                            </Col>
                                            
                                            <Col xs={12} lg={12} className="mt-lg-3"><Button variant="success" className="btn-refresh" block onClick={this.refreshSettingHandler}><i className="fa fa-refresh mr-1" aria-hidden="true"></i> Refresh</Button></Col>
                                        </Row>
                                    </Container>
                                </Card.Body>
                            </Card>
                        </Col>               
                    </Row>
                </Container>
                
                <span className="btn-config d-none" onClick={this.btnConfigHandler}><i className="fa fa-cog" aria-hidden="true"></i></span>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter
    };
};

const mapDispatchToProps = dispatch => {
    return{
        onIncrementCounter: () => dispatch({type: 'INCREMENT'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Report_setting);
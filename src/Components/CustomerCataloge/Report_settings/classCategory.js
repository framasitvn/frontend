import React from 'react';
import Form from 'react-bootstrap/Form';
import axiosConfig from '../../../Store/axiosConfig.js';

export default class CategoryList extends React.Component {
  state = {
    category: []
  }

  componentDidMount() {
    axiosConfig.post(`/api/category/all`)
      .then(res => {
        const category = res.data.result;
        this.setState({ category });
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <Form.Control as="select">
        <option value="0">All</option>
        { this.state.category.map(category => <option key={`${category.Name}-${category.Id}`} value={category.Id}>{category.Name}</option>)}
      </Form.Control>
    )
  }
}
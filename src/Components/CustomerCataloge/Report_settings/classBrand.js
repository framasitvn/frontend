import React from 'react';
import Form from 'react-bootstrap/Form';
import axiosConfig from '../../../Store/axiosConfig.js';

export default class BrandList extends React.Component {
  state = {
    brand: []
  }

  componentDidMount() {
    axiosConfig.post("/api/customer/all")
      .then(res => {
        const brand = res.data.result;
        this.setState({ brand });
      })
      .catch(error => console.log(error));
  } 

  render() {
    return (
      <Form.Control as="select">        
        <option value="0">All</option>
        { this.state.brand.map(brand => <option key={`${brand.Name}-${brand.Id}`} value={brand.Id}>{brand.Name}</option>) }
      </Form.Control>
    )
  }
}
import React from 'react';
import { Page, Document, StyleSheet, Image } from '@react-pdf/renderer';

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '100%',
    orientation: 'landscape',
    justifyContent: 'center',
    margin: 5
  },

  view: {
    width: '100%',
    height: '100%',
    padding: 0,
    backgroundColor: 'white',
  },

  headline: {
    backgroundColor: '#ffffff',
    justifyContent: 'center'
  },

  footer: {
    backgroundColor: '#000000'
  },

  image: {
    objectFit: 'cover',
  },
});

  // Create Document Component
const PDFCusommerCataloge = (props) => (
  <Document>
    <Page size="A4" orientation="landscape" style={styles.headline}>
      <Image source={props.headline}></Image>
    </Page>

    <Page size="A4" orientation="landscape" style={styles.page}>
      <Image source={props.report_item_1}></Image>
    </Page>

    <Page size="A4" orientation="landscape" style={styles.page}>
      <Image source={props.report_item_2}></Image>
    </Page>

    <Page size="A4" orientation="landscape" style={styles.footer}>
      <Image source={props.footer}></Image>
    </Page>
  </Document>
);

export default PDFCusommerCataloge;
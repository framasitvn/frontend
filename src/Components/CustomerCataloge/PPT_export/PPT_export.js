import React from 'react';

import Button from 'react-bootstrap/Button';

const PPTCustomerCataloge = (props) => {
    const generatePPT = () => {
        const PptxGenJS = require('pptxgenjs');
        const pptx = new PptxGenJS();
        pptx.setBrowser(true);
        pptx.setAuthor('Dat Vo');
        pptx.setCompany('Framas');
        pptx.setRevision('15');
        pptx.setSubject('Customer Cataloge');
        pptx.setTitle('Customer Cataloge Presentation');
        pptx.setLayout('LAYOUT_16x9');

        pptx.addNewSlide().addImage({ data: props.headline, type:'cover', x:0, y:0, w:10.0, h:5.6 });
        pptx.addNewSlide().addImage({ data: props.report_item_1, type:'cover', x:0, y:0, w:10.0, h:5.6 })
        pptx.addNewSlide().addImage({ data: props.report_item_2, type:'cover', x:0, y:0, w:10.0, h:5.6 })
        pptx.addNewSlide().addImage({ data: props.footer, type:'cover', x:0, y:0, w:10.0, h:5.6 });
        
        pptx.save('Demo-Save');
    }

    return(
        <div>
            <Button variant="warning" block className="btn-export-ppt text-light" onClick={generatePPT}><i className="fa fa-file-powerpoint-o mr-1" aria-hidden="true"></i> Export PPTX</Button>
        </div>
    )
}

export default PPTCustomerCataloge
import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import Image from 'react-bootstrap/Image';

const currentDate = () => {
    let today = new Date();
    let date = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();

    return date;
}

const Report = (props) => {
    return(
        <div className="report d-none">
            <Container>
                <Row className="report-item-1 align-items-center mt-4 mb-3">
                    {/* TABLE */}
                    <Col sm={7}>
                        <Table bordered responsive>
                            <thead>
                                <tr className="bg-primary text-light">
                                    <th>Item</th>
                                    <th>Outsole Agility FC P0/P1 SG #42663</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tooling No.</td>
                                    <td>42663</td>
                                </tr>
                                <tr>
                                    <td>Category</td>
                                    <td>Soccer</td>
                                </tr>
                                <tr>
                                    <td>Product group</td>
                                    <td>Outsole</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Forecast [prs]</td>
                                    <td>7,900</td>
                                </tr>
                                <tr>
                                    <td>Max month [prs]</td>
                                    <td>2,000</td>
                                </tr>
                                <tr>
                                    <td>max week [prs]</td>
                                    <td>500</td>
                                </tr>
                                <tr>
                                    <td>Target lead time</td>
                                    <td>23</td>
                                </tr>
                                <tr>
                                    <td>Starting season</td>
                                    <td>FW17</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Decoration</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>Colours</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>Sizes</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>Size range</td>
                                    <td>6-13</td>
                                </tr>
                                <tr>
                                    <td>Molds</td>
                                    <td>15</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Material 1</td>
                                    <td>Vestamid LX9012 T8</td>
                                </tr>
                                <tr>
                                    <td>Material 2</td>
                                    <td>Elastollan 1195A</td>
                                </tr>
                                <tr>
                                    <td>Material 3</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>Cleats</td>
                                    <td>4030F066-001 (SILVER MET 080A)</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Sample pair weight [g]</td>
                                    <td>132</td>
                                </tr>
                                <tr>
                                    <td>Item price [USD]</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>Brand</td>
                                    <td>Adidas</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                    <Col sm={5}>
                        <Image src={require('../../../Assets/images/42663.PNG')} fluid />
                    </Col>
                    
                    <Col sm={12} className="d-flex">
                        <Col></Col>
                        <Col></Col>
                        <Col className="d-flex align-items-center justify-content-between">
                            <p className="mb-0">{currentDate()}</p>
                            <p className="mb-0">1</p>
                            <p className="mb-0"><Image src={require('../../../Assets/images/framas-logo.jpg')} fluid width={150}/></p>
                        </Col>
                    </Col>
                </Row>

                <hr></hr>

                <Row className="report-item-2 align-items-center mt-4 mb-3">
                    {/* TABLE */}
                    <Col sm={7}>
                        <Table bordered responsive>
                            <thead>
                                <tr className="bg-primary text-light">
                                    <th>Item</th>
                                    <th>Outsole Agility FC P0/P1 SG #42663</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tooling No.</td>
                                    <td>42663</td>
                                </tr>
                                <tr>
                                    <td>Category</td>
                                    <td>Soccer</td>
                                </tr>
                                <tr>
                                    <td>Product group</td>
                                    <td>Outsole</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Forecast [prs]</td>
                                    <td>7,900</td>
                                </tr>
                                <tr>
                                    <td>Max month [prs]</td>
                                    <td>2,000</td>
                                </tr>
                                <tr>
                                    <td>max week [prs]</td>
                                    <td>500</td>
                                </tr>
                                <tr>
                                    <td>Target lead time</td>
                                    <td>23</td>
                                </tr>
                                <tr>
                                    <td>Starting season</td>
                                    <td>FW17</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Decoration</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>Colours</td>
                                    <td>6</td>
                                </tr>
                                <tr>
                                    <td>Sizes</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>Size range</td>
                                    <td>6-13</td>
                                </tr>
                                <tr>
                                    <td>Molds</td>
                                    <td>15</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Material 1</td>
                                    <td>Vestamid LX9012 T8</td>
                                </tr>
                                <tr>
                                    <td>Material 2</td>
                                    <td>Elastollan 1195A</td>
                                </tr>
                                <tr>
                                    <td>Material 3</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>Cleats</td>
                                    <td>4030F066-001 (SILVER MET 080A)</td>
                                </tr>
                                <tr className="report-empty-row">
                                    <td colSpan="2"><span className="invisible">test</span></td>
                                </tr>
                                <tr>
                                    <td>Sample pair weight [g]</td>
                                    <td>132</td>
                                </tr>
                                <tr>
                                    <td>Item price [USD]</td>
                                    <td>8</td>
                                </tr>
                                <tr>
                                    <td>Brand</td>
                                    <td>Adidas</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                    <Col sm={5}>
                        <Image src={require('../../../Assets/images/FB1030.PNG')} fluid />
                    </Col>

                    <Col sm={12} className="d-flex">
                        <Col></Col>
                        <Col></Col>
                        <Col className="d-flex align-items-center justify-content-between">
                            <p className="mb-0">{currentDate()}</p>
                            <p className="mb-0">2</p>
                            <p className="mb-0"><Image src={require('../../../Assets/images/framas-logo.jpg')} fluid width={150}/></p>
                        </Col>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Report;
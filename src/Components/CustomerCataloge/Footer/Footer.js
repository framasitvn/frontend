import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Footer = (props) => {
    return (
        <div className="footer mt-5">
            <div className="footer-bg d-none">
                <div className="footer-img"></div>
                <div className="footer-overlay"></div>
            </div>

            <Container className="footer-info-blocks pt-5 pb-5 text-light">
                <Row>
                    <Col><h2>Framas Vietnam</h2></Col>
                </Row>
                <Row>
                    <Col className="mt-4">
                        <h3 className="mb-3">Development</h3>
                        <p className="mb-1 h4 font-weight-light">AAbhijit Gade</p>
                        <p className="mb-1 h4 font-weight-light">+84 (938) 987 985</p>
                        <p className="mb-1 h4 font-weight-light">abhijit.gade@framas.com.vn</p>
                    </Col>
                    <Col className="mt-4">
                        <h3 className="mb-3">Sales & Marketing</h3>
                        <p className="mb-1 h4 font-weight-light">Nguyen Thi Bach Linh</p>
                        <p className="mb-1 h4 font-weight-light">+84 (976) 41 16 70</p>
                        <p className="mb-1 h4 font-weight-light">linh.nguyen@framas.com.vn</p>
                    </Col>
                    <Col className="mt-4">
                        <h3 className="mb-3">Customer Relations</h3>
                        <p className="mb-1 h4 font-weight-light">Jacky Liang</p>
                        <p className="mb-1 h4 font-weight-light">+84 (933) 36 9340</p>
                        <p className="mb-1 h4 font-weight-light">jacky.liang@framas.com</p>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Footer;
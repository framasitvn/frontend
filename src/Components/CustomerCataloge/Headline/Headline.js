import React from 'react';
import Image from 'react-bootstrap/Image';
import PageNav from '../../PageNav/PageNav';

const Headline = () => {
    const menuToggleHandler = () => {
        let btnMenuToggle = document.querySelector(".wrapper");
        btnMenuToggle.classList.toggle("toggled");
    }

    return (
        <div className="headline">
            <div className="headline-bg"></div>
            <div className="headline-logo"></div>
            <div className="headline-overlay"></div>
            <div className="headline-content"></div>
            
            <button className="btn btn-light" id="menu-toggle" onClick={menuToggleHandler} data-html2canvas-ignore="true"><i className="fa fa-bars"
                        aria-hidden="true"></i></button>

            <PageNav />
            <div className="headline-cover text-center">
                <div className="headline-cover-content">
                    <Image src={require('../../../Assets/images/framas-logo.jpg')} className="headline-cover-logo" fluid />
                    <div className="h3">| Seasonal Product Catalogue |</div>
                    <div className="h4 text-primary">Adidas PRB SS19</div>
                </div>
                
                <div className="headline-cover-location h4">Vietnam</div>
            </div>
        </div>
    );
}

export default Headline
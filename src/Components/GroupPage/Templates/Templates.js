import React, { Component } from 'react';

class Templates extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render () {
        return (
            // <!-- TEMPLATE & TOOL -->
            <div className="col-12 col-sm-6 col-lg-4 mb-4 mb-lg-0">
                <h3 className="border-bottom border-gray pb-2 mb-0">Template & Tool</h3>

                <ul className="list-group list-group-flush">
                    <li className="list-group-item border-top-0"><i className="fa fa-file-text mr-2"
                            aria-hidden="true"></i>Cras justo odio</li>
                    <li className="list-group-item"><i className="fa fa-file-text mr-2"
                            aria-hidden="true"></i>Dapibus ac facilisis in</li>
                    <li className="list-group-item"><i className="fa fa-file-text mr-2"
                            aria-hidden="true"></i>Morbi leo risus</li>
                    <li className="list-group-item"><i className="fa fa-file-text mr-2"
                            aria-hidden="true"></i>Porta ac consectetur ac</li>
                    <li className="list-group-item"><i className="fa fa-file-text mr-2"
                            aria-hidden="true"></i>Vestibulum at eros</li>
                    <li className="list-group-item"><i className="fa fa-file-text mr-2"
                            aria-hidden="true"></i>Vestibulum at eros</li>
                </ul>
            </div>
        )
    }
}

export default Templates
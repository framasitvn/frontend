import React, { Component } from 'react';
import Swiper from 'swiper';

class Locations extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locationImgs: {}
        };
    }

    locationRenderHanlder = () => {
        const locationImgs = {
            Germany: "https://lh3.googleusercontent.com/ft_aSorFGm8z4LoOnqcpB0k5bHwmt-cEM2S_32DupQvbtaR1ASPMGN5ZE_BRtyWiNLOpKkQH-4r9dFPpHX1aaF-ARktw1oTVl3W2vAMguxLX417DRfTfCl3qPRVa6ysr0nPZPalT9FqhgZtZaaGrlICmWpI5TYQJmR3bqqhefVESdWlRJSxlsJT9znHIBlthSvRwSpiQFiCXR-ezZfSYUUN7INiCi0raLdUUXdWcQ0yL5rRoeAisOITighkpev2vBOd1iGyO65EYjM5MOuzPqcmXJ-LQzoAwPfGuDKJhGdk5tJNQ-qaDkHO9oJJ0_DrKGQTnpHcXajsIZ0u4vP-L3tDW5FjrcVEl54YkoP47H8oMlXLe_EKQIC_7T5Z4GR1qGvk4Z49dC2PpcjJpg2UaurNn_wqwTIKSODBqYd4S73K6V9VaGJDV5LRmadvTBWLZNgwg1nLXqnw6Br2Z6pQDSjDfFHaqKDEEOIN1rdiLtiJClkE7Meok4N0mbqKBp73gTyyjFaBXSQO7DkCYug5zJH_aQuxe7-NP-vG3ZPWqssxPzcZmhIIYJeuiBZB9u1l3bPzdXhSDgJe74tXhDKJaKVFCNsZpjqaEI7gKLem-kE5Fw6f_rDphBwDCs2mribIa0fx8Te-Qo-DJleGYC8AfoN8ToGMPzBA9WIYGDKRg0Q7OMAmvSAuOHKnQMvZUUilMI53bNx5J8t_5gg_3BFdkXkbSY0Ve0Pftx2C1MtW9WctqYp9y=w1791-h1009-no",
            GermanyFEZ: "https://lh3.googleusercontent.com/1mYrqdghHs5716kOpbfQTrJuKmiPyni0WwuAz4YaOxMjahVqeyjYzXe_jgYFHds7U1vJ28qtBuaJTP1Va5nerzdZkPoX22bA89CZOOT3GuFhXLbtmZO8JGmPqfPl3SrvlpJf99pmSoVVz5O5OlLzO0xwprhXSWrawDGTixDIBh8GvG5-alrh0zs6nGFcW2S6c3Qd5xe_1hc0H0zdQsgD855417iY7DS5dyqRtdvRhRSvy1t_DAgk8DHjsyPlzl49ynd9Pjh_pa8pMSZ6RuL5patfFs5emUBpHJ5t3ZhxIwdZZrD0kH60KgCC0s9K_0PUjyNkMFJJxZBcf1qEvCI1mNP4V8LHPGBHlvkdqJxRqcFHqVlB89WCrSUPk7nglreuHQj_-kxX5Yl0wq6ZOWmNx4GzjmpDIC-M0P2bsoJHo3EunNWxb86LR6UCSt8mMn8p0S0bp1eHjfPRPGsixqMaA1kDF2jGR7E2wpzNpTF4ddwPUJK3D4ZrYc2ZL8WL4n4QEWfcQz1WClO7PY6J8ZXKp8h_EQJiEDBeBvkFJuhR_5YR0x85VDUJqSkhmJ713lL5e77RjfUdgfkKmCdmsbTWqygTNtsCxDAWdBLzoJsPIM4_OuIS1lvCRt21ufYof2F7rez811MDRQ6zRdBIkgTenh4oLXgBYe98hvQo97w4VUQSeJnAmcB41IrAGTDmlQ_vu3Q4BUqk_j94osMS-BDDymM2uwD3f93k1xeBoDU0j6LhWA73=w1792-h1008-no",
            Korea: "https://lh3.googleusercontent.com/peAThkniKsFD_oSuyLaAUuJIhLmSW8G_I0bOySwhyaD3f48Px0v2vDwqfOkKO4VWZSlqRFVfRvryaNe7XBoJ1Xm0lNJksiNn-ENxepqAv6GC_6zcpsAI8Bjc57jIepFmlVDB0xC0hHK7OSLSqX8YusEEoNFXRsYGMJ2dnB-P7oCWODZPh1M1HAHFJXlKJ0LCa1fcji0D5MFKUrEzC6ujtc6TGEUaeM-OH7JwVRj957JQy4PgDqzXHJ71X80AwE69tPC5uYpWIC8WsPBw4XUyXau4R6Vo4FW6PgYMHJmmtu-r5vfS5IJu7uVe_Q1llb0-ijZy7VfTArD5cCTIKc8rqW0SVfVIbbpUh3ipHUj0uSqXKyo6knIm8iaWqYrX-Ucs7irr9H1fw429R6XABc8P_kdP3YAEYt74liuwG-H80gwJ-nGKmOE-JBBVp8ZPiitQwUbY4w-lxmsK7v4TiCnwdd58UYRPBNE4DdLPvHJO9rcfMdiBUOp7MRhJtvxLOYTDeGI5Uu46DBspVTHny_MxIkvb-FFpnzWmIV7uzPa1in5mJ_EuJOv_YZiY31LBGJsL7hxnMurcbzfy-b9TgQeUjYiYL2hBV93s3AlI2gCFBr7D2jgQWIvS6M_QiWA5UmtTUqcVdLl7oOzz8Omn9Tr47uHnbvmuyh48EKKPhRNwJrOYjP97r1iN8DZA0MqVpc1skm4RTjCUu97eEDX6lundHb5bVmKOruK2xYVbmoOsPwvnoiCH=w1792-h1006-no",
            KoreaBusan: "https://lh3.googleusercontent.com/iZpxfYhDQHCRF3Jj4I6d-5Agsp8l0QqVCSvj40we8sPDlP6h_i93k7TY1SRhcdvqBuk0x1_a8ne7ZWDho9OOnk6Twg7Zj57GiwI9oJHHxp8y1_g_2gizzVvlblOeA3F5LoJ24nhYI7SDleW0tED6Ej0KAhQczCdTvJn_z1FkzWeLalivYrS_Sod3AF_qrGf1_mKZYYPR3pZTk67hI4t-AX_n_KavTcS_TntgvTIb7h-c8eMXALNAC3zX-1rkv7UDeUTwXeDSCIFUPXLZDcX_W0WhmOcxFPzKnbC4Sm9SkhzjmnSMmcqHhhN6gGLOAlNybiIYkBtP7YXLUVbyzYDhMBJk9AOlJ0oJJJLwesQPGCA1mfmu-jOpfBy6rFiOPX5t1NFk6IkXrjsfhnZ1ks9n627tRusyclIee0iSD8Agi5nKsI0Iuwk5s0gv79Oj2aSis0nUDo3_MQOh-sqDJZcF46xJ7bh3FByxmX_7Hv-D22FRdF3R_ioeD4DEwp2cGFrKcJrQ_wyVFa6oK4hF3JQNCF3Vhl6TmmOsKWfkvq3Sidif8UPxZRXUr6qHu8FqIuhCtZmcX6XxDJda94Qw0SkxWhb0V9oXCxkrvOhsbR21ryaf-8-StZ1zBfjxqwVzpRA6Qxc6DHZ1mdLnqmiN6gQFWaCXWgABA8WqwW2dZTQog31EGYJNNgk9ZszG8UMoGJOsjgrPw3i1I-KTZAc-6nF9fojO8dTvPTcQizlpdhVH3ot_D0Iw=w1792-h1008-no",
            Indonesia: "https://lh3.googleusercontent.com/TOqu0h0_ZfeBKZkMC0TcDAqE1m_cMuWW3UPe7XUQdAG3HegmMxwhmOXQMM_5rwEIA7gV0U0qy6ya4xMqVeaohfbG9-5n9cdPWGeEXzLctK3Y4ZQ-fBrTRYl7f0DSWEWqfvcAGWDtyffDqA_6R5LuIag2Qrn_dFDO0VWGGnl92KcaSujIxER82IQ2-ZaCfX61kPr4g6aAi80CMr1-muKOZaBZySpylMJEC4l60PufPmOng7UMrcLVcsYjEOWlnJ37a0O6A9U-9OnlsJIrBEECXArGScXrtXfyGOKPU5iGWHl1WcMPcjxLbEqdTysBmN59npw8IiFl-YZri7N1u-DtWotLrGNibjqYQCYxvHbYzlzbzly3NGp7OQdc9czHrJxo0TTyV8Tr-vUuSDGf_VSmbFyOX7DUhyHEIMth9bURYuGl-rDXA-aUO24Mqw9jMKOYVk6kJW5u0_uT8loeiPuLdWvO1zmrDxBEI9a_MLP7MuUBm0cx2PfT-KIZLdGFe5ldFqqB9QqXk7dTs7D-nGojfnfE_yg--eE2U7WLYdpdd3EvE9yKQvS7ZVQszEMYn0Sin_ezwpyW_CNQKKiPw0ApeZ2-fBNJgA-0llpI7oI0L1pJOWKX3vhBDN51dAvvPnBeeY1gZIIKgLSFMoLQ41TwLBk2VscrY0GDNesl8qIOU2Xevd7TAhhG9Y_EzWDJ46HT1xlks8xrayxYUdItXb_HpKIMxYI6d16NO7PqQGjc2ngGwkDY=w1791-h1007-no",
            Vietnam: "https://lh3.googleusercontent.com/YLjwSjJEEHwV6KmNCB67Pdk3dih01OlmIy_Rh9fQIHwiqllV5-x0bzCp_g49tcsVBZLR9UA6wKtYyHti_Bz7CjOxNV-Nzlri3ftyJZVTgfl86DhtToRjlLLNoYt9V72_BdARj8IE8HTtdCi5WxQBDtfLLJ0izxx78abqTgczwXYP7daihRPjLh2ZTTGJdxt7yiHPEEIlXw9iDzKbKLsUkrB-VN9V_wdsQ-6COncaRyzHR_vcab07G9m-au2Ria_oFH1koLtt_04fD1eRA0bmATxoNpnXv2V1HusXhWWkQWtMfAZIFZjwsfUG3UmN6_xp_J0FMMPRSesnz0SZrfrqPOgAjRmneKlkXJELRHp4xJs73jGNvYp_4jMYbbRMdDR_T6lnVpuLGLWILVCRsrBAGQSXHUSaYjOglgzp61UmucM8fP6DWvea_2QAe4LF5E5Q3jihHnKeoOrmAOdDdJZPAgVkvG0JQYqNAWppawmVQIbFbv2ULvOgflqqB1dyi2a6kyPJo1XfL7wwmE5-gTx-3tcXoUXnQIx21pKr8dOHXWT7FZ_dmmTv-uPyOMVSXYz0ETrgv2uVofFwS_7wSU7HtdrAhVyirW6dnzvUAbuWlUZls2JQMGmVn-lW58mqoaGempw6sPXUE5jlOJxC83w57Gg1yaXuxSRUEkU6EQQpFZH46Fp8r6nyNNzpTTzljJhkfEBY5dvTJunpI6u1nobkGSDgP0uBCYrOkx91BJ9lPErDnq8p=w1793-h1005-no",
            KoreaVina: "https://lh3.googleusercontent.com/3X8fpzrkK-coI_xfIvzHy-Wwn4SIAOR9xkwAzJ1Yeetu0F6iKp6QE6b26R-uiHaL8hyRCrTlbFCHv81cuq24ebxol4LkXur-Wmt0PRVpnx0A0F1DK5tyXz7FZxaIUNW2HIRzXUS8JZrNO5MZHdYRLWvzJp4W5fRCrSq-PynokdJmSC7On9FYgJPP89kXh7cYnCv6-1MuLxTagGifkErBYdfgnUr2NiqLdcul6ZE7F6N0HrsN5nBVhq7EsNVdXbF7_qiZsmMLb6z4sjBKhvdogHMQmE7-jUFrkChPmi4ccvBotLsjRumoZi39yjYmjdbq9LBrK9QVTVtzM0aq29g-b61SiwqpserXecM2xnb2YKc_68wLbxyB7byvbKDPaP8Fs5Tq4wkGSeaGOaoznFoirgFVgQz1GGRHw1kcZNtIFK58pa9EuImww_x8y5ALzNo9-R105AgbNZbzMS1gmJGuL5_KZiaFQ5hbWV9CvgEoz0s4n52vvLjb1Yd1zGeVGjRrzI2Alibu367_WXm6CxXtb4ETRyecEUsCa6Z15nBAY6VATk4rJ1aonFCtObCXVX1Ghqcc2vcZH105v3TrNZ4wW_aFFPzWf9WTOxt4ZYbWLLSvN-bboNUa4smhhO_fcPLwM0xpOy0zGVvBZd2-v8xFkSGeBNNWhdM0ON6LR5XruOzG-KsWkNyitpKIcxA3U05UvnlIcXTRnYU0jKJ7xoH4KJQgiOJCQzpMVeujOHRFCBkEnWwH=w1791-h1007-no",
            Hanoi: "https://lh3.googleusercontent.com/66lFGuKAwqMVSOwEZ0LVrshZxpmsTChnYc4gBvR_QJpukU36y1sdivnuM_423YWvsT9MQtjIthX0dfoL8GnAeTfscwojZ11QlYz231w6QD6wNrk4gqdWoWjBS06P5ZBCifrm-N27e09AQGXhiin6309tu4-7-UietmLwsEzdjSHB7haYuWKMx5fygNpdSRtQg349l4_HYCdDxTmkHDJVhZ_Og0nTysF4fCLx7Sa4bcJmVm3baiLZo9wtFTvT3yxcEXnUvgKyuJ301dFI20r76pgwHjJF1BMCCsvbubFOFmwRfMUFIOm1JyaYbBNgEVBzP4Gfd9K9WuyTZT5wBdZc9XdTFWSc5KGwdDQn7TzBuXcWFf2EAkKVtPJfuRltVXvkA8mX1Y2nTYMXNqpL8xTb5fqyu7mKc83TvFiDkAA_8MCpkxUS52GpkMW-_PWOQBR4VcXrka7YqXxOxWJ1JjzJ2GDmprTbyOg-yXaFE1irKnFIcCYDptRmFx8HisrPfEmKPQkGFAiIPexSCC-v2qjGu1yaqRww4G1vEPmte1682fDcRHQmf9w0MCCLnXN2lSTaDyZimqWw0MXxBtTn0rAlKsMurAo7JiNgqXtKwhBNX3YEDyuNPnc_4yeVUlhnC3ioMnL-K7onAUP_ZeDHMyPjCTBR3jtk0KdXFSYLUcXF9uaxxSvxwWtT6W2yfSXSrvGyyIRMIpjAM2OQWYqm4mmy0ZB8AVMb8vMNTtakLz-YgkEDTYLi=w1791-h1007-no",
            Hongkong: "https://lh3.googleusercontent.com/MuDP4ogBNWkQFiSFx6XbkR5KMVz8yDPwK--LWVR--P3Ak6sc056WAjRNc-vqU8kKo7fgEa2alpWZ00e-C3_slzmO6WICNmw81OTRY2cqB-n4nCoxCdJWVzoi0oYC1q52jz479mvjxOQZgI3G4mlC6c0ghN2FvcE2RM_Rn13Ifptpt9Yhf_wnKC-h2tO5M062yFfBRRyo5aVpnlVamzDs1hXieTYk3KKYZ2MnMUWp19DwdZbjpF0ARW3RzsX3fhcbIRS2Pv5jVWCRMW_FL92KHQtm9XKHOwu-i2jyYJp4qLX7lIGuFt3ZH08I1IFJ2-FsEThNougEuI72rEQ_T9kL7flzcLLP1-Ev-Tc27ZL8jwL4Sk4bK5yiJFnk7hi22Fwq1M-4GpdDVZE-khakMxjw7waNvOsHETETrbhOqdSSvuPyS3RbSDu62dMw_RHe3a3IuY6dsaMT_76evxpQBjGcVnMl5VnISXRQrN-x_lm5bkrlYA0HOUvS9fdMqJ5QnK3qW5LNmthO9Ib7ufxrBuwQbdF3kPtKcv5sWgWBvljA2llHSBvlWdC3_3w90EsKNyWV4CoBqeFd2V5DfHinvhdlyV0bvgxYakXVOW9K2_e2Y-Yy6qcm_564r2vyWZtoEfksWcp58eL_potqh4nuTH39CzLmoSSNmiXNw1oFEiBLtssir2bAefjkk0C3-A0HUwhu9g4sOqG7eH7vAJf04Jcgh-VdXfTgVe0_t-wt85B2rS7MR3m5=w1791-h1009-no",
            Fuzhou: "https://lh3.googleusercontent.com/DWkxgliwjeYvMaaXelWIMFJpcElFGcvyr7KEa99s6vAaRhryxqcLK9rRzwcB6S22AbLa7zt1PU0GEyG0RwUiSJyhwZ6h2wA6FZXnCmton2b3WqfCNQx7_J8mB__8X26eWIn2BQvB3pY9VV7KqK3ZEy9-rys8YuXdywGbeqFI67PazrgXI3PhM6e5KkD-5eDybRlRFs4-OU82vSr4S0tVsi-tpiU0i8v1Cj33cIZVqE4Oidl7spat5o_FLyVagjr0dPqi67oJuxQWkk_NGKbv4Jm63qXAPBqfG9m2-0toxJvWFR31LVTRLWwMmgRAPY3VNpDKwt3L_eqfK9go4x6sWgvIEtCt0PgWTVmK_lz39OP3OUr-kch3Cpalk-EbSbBL4Ek6E-cIvRCLvT3GW9nyJbCK2kZxGdpWG9ERAL33flDpMHN6G2BZGnTNVrZAFVmPDPe-a1BpLS6eQgWXe3pS5kUQZr31CUVNlNz5cbctGMvpHGAEslqBOLxrQzUhSYqoKps-iV3WPZHequrGmoHKCFr232zDr1djOfuX0Wv3s9IsoincTwDDU1tqEaHaDZawSup3xri5A15gZQVlvYFJRpbQcoGuE4qt3Yqs-mqCHPxMNHuG2UcjYKtZQaTs5U046CPuEsTsBRJZVK5MDXNISjVTDfO_J5WKoh8KTbqGC5iqO1CNOZMBbD_AaJXQ4RHveO2tXSltZSMLLLOc4hFu4lms61mXHrc5oCAd4IRwoblqxphp=w1793-h1008-no",
            USA: "https://lh3.googleusercontent.com/mKpxYWonefaCF40QC6wLDRkUmP6g1LNulva0ZtvWC0_dWfvuAV8jrIrRz9BSGZ7JWMWiNH9cvukxPtYbpTgavGMcZD1Lo8RBCivg9kod1XOG3dBM8n3RiaTWOZ6A3M5vOjOkcQl0o80qKfEQVwg0MlMOsyJI4UkZkjMBozXBCdKDmjfOJ6n9BGdbejlP7_yBNKAlYk3FJtxDfXkpRARSvT3rcR9tXmzfx1vyAf_1uHELL3nkfubJ7j4OG2uZJFt2xmT-agDOsun3OZKZvOOpSkOp0zbUilb_H9KxBCMVc9G5-GSZ4RZ4c1UMy9hVH8tkJQmYpcw_aFATEkJwBNRXke8HKWp2EjgEXBMnFB48WD0XSiRxLcJUbNYlthNOehP9stKqUzXRBSZRyyfxEOqGPpcKwgc4SIhGz8aX0vek9rZ1BWo6FX3WjAzW7QapGG_6iE-h6mEADFyEt-OyhQYDvhuQ1YJK6LWS1KIgd-k522uCFqpmAuP7XwMsDU4gV40w16qTBrrRiTruZI6Q3RZUBPdAczUpwSK5usWljQP8tfdwvcbVUoz7Xf9zaQnxF6UqRjACjnRBPfitjyIDUTgqFgp55yHGf5H2VHCao0m3ySdy_V2rt1TQ80J7INKcv0b9PkxFwZqukE7tSRiOeUPcHSglMW6CF8QfrJ_U5o_pDVlNq3czu4ZS2_-DEbAmoE0g_8uRkQTOHlAIY3J1hVMxp3hzczO4OEIA8T4vBlmNqnzVps4L=w1792-h1009-no"
        }

        this.setState({ locationImgs });

        const sliderWrapper = document.querySelector(".locations-slider .swiper-wrapper")
        var slideJSX = "";

        for(let key in locationImgs) {
            slideJSX += `<div class="swiper-slide" style="background-image: url('${locationImgs[key]}')"></div>`;
        }

        sliderWrapper.innerHTML = slideJSX;

        new Swiper('.locations-slider', {
            effect: 'coverflow',
            autoplay: {
                delay: 5000,
            },
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
              rotate: 50,
              stretch: 0,
              depth: 100,
              modifier: 1,
              slideShadows : true,
            },
            pagination: {
              el: '.swiper-pagination',
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            loop: true
        });
    }

    // CALL A FUNCTION WHEN COMPENENT FIRST INIT
    componentDidMount() {
        this.locationRenderHanlder();
    }

    render () {
        return (    
            <div>
                {/* <!-- LOCATIONS --> */}
                <div className="page-locations">
                    <div className="container">
                        <div className="col-md-8 ml-auto mr-auto mt-5 pt-0 pt-lg-5 mb-5">
                            <div className="section-description text-center">
                                <h2 className="title mb-4"><strong>Framas Locations</strong></h2>
                                <h5 className="description">Lorem ipsum dolor sit amet consectetur adipisicing
                                    elit. Consequatur, commodi, dolorem eveniet vero illo assumenda itaque
                                    consectetur nihil.</h5>
                            </div>
                        </div>
                    </div>
    
                    <div className="container-fluid locations-container">
                        <div className="row">
                            <div className="col p-0">
                                <div className="locations-slider swiper-container">
                                    <div className="swiper-wrapper"></div>

                                    <div className="swiper-pagination"></div>
                                    <div className="swiper-button-next"></div>
                                    <div className="swiper-button-prev"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Locations;
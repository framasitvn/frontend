import React, { Component } from 'react';
import Swiper from 'swiper';

class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newsIMG: {}      
        };
    }

    newsRenderHanlder = () => {
        const newsIMG = {
            slider1: "https://lh3.googleusercontent.com/J16GDvcgmekGSmvLIQgssiqb0fyr1gUKqWcoENoWPR7v3OelM8NA-odM5JpVx51EGJkL_IS6GXcBGLHaxuTuOZgXAw2-GOTUM0FMPLV3TrQFgqZ5MWcGVBlocNQfa0QyyZgUQ_uQ6K2-lvJk-GtbHDunRIbdIHXvhKaLdnopEXCvRllAjaSoH4zQnjTeZmBWJwhYYkQru5Mw8dzKyHBljd0izSZGrE2ind5rfYrrV5e0FS0GSeIROJmGS3CsHvl5UBqwOPacVnB0XQbNOmHL-9zplR1k5Xbq-lLSroZE9ta_smIS9kTQwdR30uBLGzJIz0HHtFsB-pBy9Wdr3nR_Nba0tXrye6gx1I92jwEYauEsk4DBz63BcOTv9TO0FUwHmByN3wOsf52LSS0AayG73S3qogHNA9HFFaMrXCgTJbLbr7x3VwdULcUzxnTrnwiWJOs8LPGEQhXF3OF0IgZeDARNjWnuRrg_R3F7zbCeZ5WMDaNULBPOqlNPoGT46B1r1eUnDcsQXdMKYLMMD6_spQ3m-3VlJU1PNJDtmQM8r8ruFmVDWUsFaFKjE91lvsYgw0IXUb2-WjWeIOLk48llm9nFCN0nXSnzKJ1DZ46MfLM2Tme7f9a9icuD6PeyJAc1uFufWLSzIR16IeEIuBaIw3fpru1l-GhihMys5KCS2MTfngT7k_u-qGMlLZybDi3HjwnxIz8zt2XfVTk7fV-X-mJmXQ_02ZzdSX3n5i7Atl2cnmdb=w1202-h801-no",
            slider2: "https://lh3.googleusercontent.com/GtINdiLUjYgnigBGozULH25JjGhF2StKtptLhJ5jctwlwsg-ygnMtSqYhgpnNbMdLlUcXVu0Uy76_KcyR2uEIJ1E_6fwsqBTX4z5spQo4rFCIYjs1kWbnp0CxeUg_6LH6b7FgN3ZyBj9LuCYwnDY-OmaZS0rguaI_MX8fFMYd9VBCdSwVgOl_DiupdSEGNRnxcRNGxvJvDmHKeeJ6RxVw5QtnPkv5qneGx4SkKYOQp4FE3B-T6QxeYKHgJ9E3dw9bCd0MTpvXDvr3U3I7ZxLlBSOPBlCuWBHMz4mCiTmH-BP6MyWKnJC6I38DVi9vYrfpgcJLNHkjYkQLschDgYRvTKy5ydlapbghTXWoWzvTOaU0qg43KaXlVVqt5MK3qQN5ctlRed29YG03lsro_RePShYoeI0Dw35BZ4GePoYuMHtUxhPi1rmrXVsrLi9q_z5xZ8xZ5Vxps1aP6FKGUE4FLKpiRfXyUVdi3s7r-GV7eT9wSCepZdDBLjfBCEe9ZXHbGxCxAfR_5J0rXmJhlyS6B7HIDlAj79bOaXTCW51DeZFrnj5z7HUGxPI_2Tmc6lEA6KGKHfyceQ6g0AJmcR6OdO2KHUW0cyybA6Aa7rex6qteIv1GFPUApR_J2ziy-sIa6scpr2VbKQO4DX945WVXzEj0ewYsNjEVi5w-cDDPVBASXQeogC3OfsAKaW-47p3AVBuj7u_WD8xO-8rhxdZh4PFrbveGS0-KEz1JxzO8hzKQq3g=w1371-h912-no",
            slider3: "https://lh3.googleusercontent.com/QGdv-06swe11lz_2d8xPQBK5nA8rzW0bj_xHGHv2vNONMTxb1RSq8-iRpDIkSqqAJmlIg6EoBIU4dmSI2DhkOQUjUI6zjM80cBW1ShRQNhoqf-QAmmy6TEtkwzvXQTCdfF0SbGK4nvT28C9DmLMClHfSzot3I46VNibtPzV_-Lo-iR_rWJ8IHuug81r44-7r7lZcrQVa2tNcpehASOnHH66CBLNxseofJLguMUHobdL-fOZDVRPKnGKR_j5-iIh8OIGnmAqC10mQBMBWbJAjhBkkF3RjSf_bsxBz_r9KXva7LPUk9j4ONhehq9CQu0x442uug7gbc2EXCiuhL8-1Jp8gsLrBcYZa4gsx4o23FMeG1u_kXLPBfMM_FiWTk8r9uIC4U2UDTS_rQvA7lzFc-hP6a7vwAd8ZqqdGWCeuN5ymhaQa8Z5UWWHiMzrIO71IqA0z9NuvsxG2Ow6tU2OdG9j3jyUQMH_x5DUlTC66Uf0qrGAhI_0AaSFf1LdsqOkfUmAOKNDAqmUp2TETl30jOP4IWHX8w19ZPrmxYAtNnA_yNVqSaLlHjYjG6jDkH-Z432rnm42M2AiZHWGmxPXFbTE5xr3AQdr7jsbrccLmwceAYuRTBJ_6X-eYTRaS5A4_P7zuQoYTE68qmLJIin1CsSdVRRFs3TfxB1JTeg1yKik2BvFqDtHyFzTM-1O2rHHKXKdnnMuC2i7rYe-AfdXHK9P4zKAo9yHctdbMQbX_mIu2eMLG=w1278-h879-no",
            slider4: "https://lh3.googleusercontent.com/Jtk5VoLaBzgpbxd0TmXb7y2ixJM5-sT9OiMrBwqsdh4qHb0x7MQ-RrQZgbuzgvMbhhBm1qX66R9LYdmym3h3tkrNdqbZl5TlWeddCqQqShUW6tkF5qzJTaApKk3TPidxF8aIqjfqVvQvPmfmaLozsGf0CZ8nxzvNWo4O3R8mhH1RrhpJ7tF-0_Wi1gFD71JbUpkLqAvO3n1SyyURVRZed8g4-33josNPzFdMn6f7s91Mf5aHI8H1DoWJ_m6K364jWcaXOqaVlM93QbDfGbPUvN8TSNpCQce-1dTUFt1CmEi7J8Ar2M1XgFRhIFZtNc40T6Rfs-8lbM9YF4KI0FAVhxsF99TzXwjTk2ZghWoXDe9oWod-GxIsST1wspSVYf-wPbzzf73m6Xdpkn9-ic8m7e8RmsHD5iAm2cwcgsRkAZfNzyD6BLv49l7JSBQcYs7I7I7FU_hFtiQvlxqDNMjcmxXcgdRln9tTurRcfKSCLIXD49SVq0MzhQs7etZBKnjy6GM5-5gxvrvZHFQtTXr4plYjzw2rm_y_mh3O3ZqiJi9Fn7ItGUJ2IWC_qtiEF4J8E0aW-6lx4IUaonbV-pqcaWCRreB6YrFvq76yfLjAjwQTUC1odHRttZgD7Zx-TEx9qKCtKmQ-9_ng_SXm_i5l3Lyj8E2CxmeCY69fxAFx227UYGUhE3gfdxxx9lZsV0XqxS3pBU-H0CRmdvYFPCpsL0y4tLiNB9grEtz8YnMsdeJPM-PX=w1278-h879-no",
            slider5: "https://lh3.googleusercontent.com/Y2eXyAp5CxXP_bSlh9p0i9RgOPUdL0usBSn8fEd5xQ1Iq3fiHYbW-_fhxO0mq4OLgogb7s3kV5uXcVACHJboxrDACb_yoqPV4HLwSet49sq9ezN9qN4TcpPVaKzWz7wZ3CSzySUTTZt1PCNgVyLQSzUNo-sDUmSsOJtZKM7DHsNBy-z74hSKB1fO5AHGem4M1wLz79dH8JUvBgY4wvFzxuEbwlXUh8IxG4nbhblOiJjf7WwN4Aaz51hF07u97wZgIWXhJVBNmjk6tMx2kshKCB3NEpBAz3IeHn8L7hBIF_VCmP58ggEiRwMV-ArVUGaCYqKXzR8ZwBa79-fDqJ7jyp7TUNVMgBoqdKednpJ35ddZtGceo3nMmCXNg2RWvxXlgA58QsYjKXR2OS8MBPSRn1_ZxKL67XOwFh92G5eqs_0AendqAwLyXxlmhui_0iL9bfM-80RSNzogHdPDPiD4JhI5lZAacDnkHh7TXwc79vJZ3vCiYuM4Emr__nT7sjtTwaB-g9WSA3HFqkvmpqRILpt05jC6GQDzStKG3wiirJR4EKCr24dTpnptN3PqdA0NxngdNhAj61EmBjWfcdXlvLKJYK2L_QpJsBtuhpViTXhPrcTexD-G21ydbqHVj7ZfwwaOPiHf1VtOyR1JgyuJN3PcEtC200_uU8zhG9dlY3R9I0JBioN1HQecZFsHvYWceiVFhAntl6va1JG95HMg0CrLaYtF_2w05oPVyqQbWMmyaxNQ=w1600-h1065-no"
        }

        this.setState({ newsIMG });

        const galleryTop = document.querySelector(".gallery-top .swiper-wrapper");
        const galleryThumb = document.querySelector(".gallery-thumbs .swiper-wrapper")
        var slideJSX = "";

        for(let key in newsIMG) {
            slideJSX += `<div class="swiper-slide" style="background-image: url('${newsIMG[key]}')"></div>`;
        }

        galleryTop.innerHTML = slideJSX;
        galleryThumb.innerHTML = slideJSX;

        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            slidesPerView: 5,
            loop: true,
            freeMode: true,
            loopedSlides: 5, //looped slides should be the same
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });
    
        new Swiper('.gallery-top', {
            spaceBetween: 10,
            loop: true,
            loopedSlides: 5, //looped slides should be the same
            autoplay: {
                delay: 5000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: galleryThumbs,
            },
        });
    }

    // CALL A FUNCTION WHEN COMPENENT FIRST INIT
    componentDidMount() {
        this.newsRenderHanlder();
    }

    render () {
        return (
            <div className="page-news">
                <div className="row">
                    <div id="slider" className="col-sm-12 col-lg-8">
                        {/* <!-- Swiper --> */}
                        <div className="swiper-container gallery-top">
                            <div className="swiper-wrapper">
                                
                            </div>
                            {/* <!-- Add Arrows --> */}
                            <div className="swiper-button-next swiper-button-white"></div>
                            <div className="swiper-button-prev swiper-button-white"></div>
                        </div>
                        <div className="swiper-container gallery-thumbs">
                            <div className="swiper-wrapper">
                                
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-12 col-lg-4 mt-sm-0 pt-sm-0 mt-5 pt-5">
                        <div className="page-annoucements">
                            <h3 className="border-bottom border-gray pb-2 mb-0">News</h3>

                            <ul className="p-0">
                                <li className="media text-muted pt-3">
                                    <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/Sb_hmODdChdDhHMI_6fc0CfC6JVTa6HfuEzMWCNGEZKhpQmil8CbRqN2wEp6dbjsKoYHQUnV741tIROOIoZPhLwl3cIYyv_sxsdxmwNvLfG7w2NK6On8YOiTfsE0tXwzC4FLonPjW_7pbcLmBKWW1RNuYO_UdodwdpzmmNooHNkJfY9rpmYOqW04j0gMIKQYdAT0TWWdvpennIism_Xnfjxnb-pwnvRroLjCPvWkgLFDpCBQmGHi5GDuc4CAzTKg_pGh8-aqOAvJYqfn1hQhEppzVioYMicZKEoYjc3FCJcmqt9ncRFqcJeonSbKFCwp1UtYG3IBpLxi9eQS_4X-uGRvpj1kKwEo3ODMtgjirUG7NWdDl0M_WJTzLff7ArfTGIVstvSAzxj-r9vouqYWwA9zYfeeZ4cWfUUPrjvTAzV4NdMGF3dki9joqOwow6t_mix6cMI3KAMECDZ6zlQFY_5MvJ93pJTsfF1tnkEvr1wImUrtu2CUd8o_194cWGrRLoRSTafwDtutOftGeNADAR0FMGZLz7ZTr0zMK-eL5j6z4ez_28s922rPQ23UjcWmyus19eUgjpUqujIZI1pWPKUXU12DbDayNHojNDQFdYfqrF_Z23lVw80khMd9o_22cvQNYMTLYZFMX4b4wvX2zuxuOArJ6fmYLo6LaJq5Ymnn7eZH7QHd-H-CcFHaVkpxvOiUlS4w2mVWygZ16_H6XV04Asz5eOqgPdhXz6udVCknDmLN=w1634-h1089-no" data-holder-rendered="true" />
                                    <p
                                        className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                        <strong className="d-block text-gray-dark">@Announcement 1</strong>
                                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                                        tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </p>
                                </li>
                                <li className="media text-muted pt-3">
                                    <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/Sb_hmODdChdDhHMI_6fc0CfC6JVTa6HfuEzMWCNGEZKhpQmil8CbRqN2wEp6dbjsKoYHQUnV741tIROOIoZPhLwl3cIYyv_sxsdxmwNvLfG7w2NK6On8YOiTfsE0tXwzC4FLonPjW_7pbcLmBKWW1RNuYO_UdodwdpzmmNooHNkJfY9rpmYOqW04j0gMIKQYdAT0TWWdvpennIism_Xnfjxnb-pwnvRroLjCPvWkgLFDpCBQmGHi5GDuc4CAzTKg_pGh8-aqOAvJYqfn1hQhEppzVioYMicZKEoYjc3FCJcmqt9ncRFqcJeonSbKFCwp1UtYG3IBpLxi9eQS_4X-uGRvpj1kKwEo3ODMtgjirUG7NWdDl0M_WJTzLff7ArfTGIVstvSAzxj-r9vouqYWwA9zYfeeZ4cWfUUPrjvTAzV4NdMGF3dki9joqOwow6t_mix6cMI3KAMECDZ6zlQFY_5MvJ93pJTsfF1tnkEvr1wImUrtu2CUd8o_194cWGrRLoRSTafwDtutOftGeNADAR0FMGZLz7ZTr0zMK-eL5j6z4ez_28s922rPQ23UjcWmyus19eUgjpUqujIZI1pWPKUXU12DbDayNHojNDQFdYfqrF_Z23lVw80khMd9o_22cvQNYMTLYZFMX4b4wvX2zuxuOArJ6fmYLo6LaJq5Ymnn7eZH7QHd-H-CcFHaVkpxvOiUlS4w2mVWygZ16_H6XV04Asz5eOqgPdhXz6udVCknDmLN=w1634-h1089-no" data-holder-rendered="true" />
                                    <p
                                        className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                        <strong className="d-block text-gray-dark">@Announcement 1</strong>
                                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                                        tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </p>
                                </li>
                                <li className="media text-muted pt-3">
                                    <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/Sb_hmODdChdDhHMI_6fc0CfC6JVTa6HfuEzMWCNGEZKhpQmil8CbRqN2wEp6dbjsKoYHQUnV741tIROOIoZPhLwl3cIYyv_sxsdxmwNvLfG7w2NK6On8YOiTfsE0tXwzC4FLonPjW_7pbcLmBKWW1RNuYO_UdodwdpzmmNooHNkJfY9rpmYOqW04j0gMIKQYdAT0TWWdvpennIism_Xnfjxnb-pwnvRroLjCPvWkgLFDpCBQmGHi5GDuc4CAzTKg_pGh8-aqOAvJYqfn1hQhEppzVioYMicZKEoYjc3FCJcmqt9ncRFqcJeonSbKFCwp1UtYG3IBpLxi9eQS_4X-uGRvpj1kKwEo3ODMtgjirUG7NWdDl0M_WJTzLff7ArfTGIVstvSAzxj-r9vouqYWwA9zYfeeZ4cWfUUPrjvTAzV4NdMGF3dki9joqOwow6t_mix6cMI3KAMECDZ6zlQFY_5MvJ93pJTsfF1tnkEvr1wImUrtu2CUd8o_194cWGrRLoRSTafwDtutOftGeNADAR0FMGZLz7ZTr0zMK-eL5j6z4ez_28s922rPQ23UjcWmyus19eUgjpUqujIZI1pWPKUXU12DbDayNHojNDQFdYfqrF_Z23lVw80khMd9o_22cvQNYMTLYZFMX4b4wvX2zuxuOArJ6fmYLo6LaJq5Ymnn7eZH7QHd-H-CcFHaVkpxvOiUlS4w2mVWygZ16_H6XV04Asz5eOqgPdhXz6udVCknDmLN=w1634-h1089-no" data-holder-rendered="true" />
                                    <p
                                        className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                        <strong className="d-block text-gray-dark">@Announcement 1</strong>
                                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                                        tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </p>
                                </li>
                                <li className="media text-muted pt-3">
                                    <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/Sb_hmODdChdDhHMI_6fc0CfC6JVTa6HfuEzMWCNGEZKhpQmil8CbRqN2wEp6dbjsKoYHQUnV741tIROOIoZPhLwl3cIYyv_sxsdxmwNvLfG7w2NK6On8YOiTfsE0tXwzC4FLonPjW_7pbcLmBKWW1RNuYO_UdodwdpzmmNooHNkJfY9rpmYOqW04j0gMIKQYdAT0TWWdvpennIism_Xnfjxnb-pwnvRroLjCPvWkgLFDpCBQmGHi5GDuc4CAzTKg_pGh8-aqOAvJYqfn1hQhEppzVioYMicZKEoYjc3FCJcmqt9ncRFqcJeonSbKFCwp1UtYG3IBpLxi9eQS_4X-uGRvpj1kKwEo3ODMtgjirUG7NWdDl0M_WJTzLff7ArfTGIVstvSAzxj-r9vouqYWwA9zYfeeZ4cWfUUPrjvTAzV4NdMGF3dki9joqOwow6t_mix6cMI3KAMECDZ6zlQFY_5MvJ93pJTsfF1tnkEvr1wImUrtu2CUd8o_194cWGrRLoRSTafwDtutOftGeNADAR0FMGZLz7ZTr0zMK-eL5j6z4ez_28s922rPQ23UjcWmyus19eUgjpUqujIZI1pWPKUXU12DbDayNHojNDQFdYfqrF_Z23lVw80khMd9o_22cvQNYMTLYZFMX4b4wvX2zuxuOArJ6fmYLo6LaJq5Ymnn7eZH7QHd-H-CcFHaVkpxvOiUlS4w2mVWygZ16_H6XV04Asz5eOqgPdhXz6udVCknDmLN=w1634-h1089-no" data-holder-rendered="true" />
                                    <p
                                        className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                        <strong className="d-block text-gray-dark">@Announcement 1</strong>
                                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                                        tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </p>
                                </li>
                                <li className="media text-muted pt-3">
                                    <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/Sb_hmODdChdDhHMI_6fc0CfC6JVTa6HfuEzMWCNGEZKhpQmil8CbRqN2wEp6dbjsKoYHQUnV741tIROOIoZPhLwl3cIYyv_sxsdxmwNvLfG7w2NK6On8YOiTfsE0tXwzC4FLonPjW_7pbcLmBKWW1RNuYO_UdodwdpzmmNooHNkJfY9rpmYOqW04j0gMIKQYdAT0TWWdvpennIism_Xnfjxnb-pwnvRroLjCPvWkgLFDpCBQmGHi5GDuc4CAzTKg_pGh8-aqOAvJYqfn1hQhEppzVioYMicZKEoYjc3FCJcmqt9ncRFqcJeonSbKFCwp1UtYG3IBpLxi9eQS_4X-uGRvpj1kKwEo3ODMtgjirUG7NWdDl0M_WJTzLff7ArfTGIVstvSAzxj-r9vouqYWwA9zYfeeZ4cWfUUPrjvTAzV4NdMGF3dki9joqOwow6t_mix6cMI3KAMECDZ6zlQFY_5MvJ93pJTsfF1tnkEvr1wImUrtu2CUd8o_194cWGrRLoRSTafwDtutOftGeNADAR0FMGZLz7ZTr0zMK-eL5j6z4ez_28s922rPQ23UjcWmyus19eUgjpUqujIZI1pWPKUXU12DbDayNHojNDQFdYfqrF_Z23lVw80khMd9o_22cvQNYMTLYZFMX4b4wvX2zuxuOArJ6fmYLo6LaJq5Ymnn7eZH7QHd-H-CcFHaVkpxvOiUlS4w2mVWygZ16_H6XV04Asz5eOqgPdhXz6udVCknDmLN=w1634-h1089-no" data-holder-rendered="true" />
                                    <p className="media-body pb-3 mb-0 small lh-125">
                                        <strong className="d-block text-gray-dark">@Announcement 1</strong>
                                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                                        tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                                        fermentum massa justo sit amet risus.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default News
import React, { Component } from 'react';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'

class Calendar extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    
    render () {
        return (
            // <!-- CALENDAR -->
            <div className="page-calendar mt-5">
                <div className="row" style={{backgroundImage: `url('https://lh3.googleusercontent.com/KiswNwXvTuQiYQxjohKpTHAW1UpqtkqR-b4H4dcPR7doQdtZo5VInmuA0dXiYHR5mQMnyOiffZWKqy_V3GyOs7CKceSuEqzXWyLH3zXeDnCNCxxKmwUyPiKFqYzroW1wTCEjIJyGBn1KBCcinNpKTtqd1sMlp6Yq4SzOG-mDKa4Mv1HzA1yu6x9WnklaNqw1KoNB16SuZVdmW4iwKRaw_FGzSLgsfwaYQdB_LJjBOQoHt9YvAPp07wQQM0o7VF_GNq4L7DxHJ5gnn8fMRoBCYfV42aCHwHBB3NdNxXFC_thTMC3z7Sc3nvpxHjrh_mOo0y_-f0lXAhuQ-9ACWwGB07Bd3TVBkGOQAonGDYbwkmwKOwFSVTMbvGg-QywI1cIOiRZOgR4BNJz4i9zTC_xuqOAi_XEbsA6D_4t4kTIx8M0HmP-k5vFz7Am0L-429lxzBc-1CypAff_2d5dAdKon7cPTivjFdTQMpdMBcWUPWiQnSGmj3nGGkqRSZ4ZTe7Yo8pqWiwuOt51y_GxvA5znzVQ99a0cIjIVaRNkTJmpkUFamE7Cxxq-lupW7jIzNbZLmj1WBFsV_ehUMRtkW6orZkMhcY3w_x1UkjTIMOfWwAaXOVCelXfas4qEF4FQ0W-jeg2TXeKe2tpuonJpsEDMH36WD2FOc4so-apcTKOArdcWRfc37-gAtNSumz8sXbmolMhtquKokeVfWpYcda1uQadj3tKhTvXZWdtX4N9Cq94oamlZ=w1278-h879-no')`}}>
                    <div className="col-0 col-lg-3"></div>
                    <div className="calendar-container col-12 col-lg-6">
                        <FullCalendar defaultView="dayGridMonth" plugins={[ dayGridPlugin ]} />
                    </div>
                    <div className="col-0 col-lg-3"></div>
                </div>
            </div>
        )
    }
}

export default Calendar
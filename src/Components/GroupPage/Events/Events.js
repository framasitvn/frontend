import React, { Component } from 'react';

class Events extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render () {
        return (
            // <!-- EVENTS -->
            <div className="page-events col-12 col-sm-12 col-lg-4">
                <h3 className="border-bottom border-gray pb-2 mb-0">Events</h3>

                <ul className="p-0">
                    <li className="media text-muted pt-3">
                        <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/qrz2VeUsUDTZ3JaT-UsdGuIbvOyJYKTz5h_P7ZIF_T70Qzgg1fy2eOeOB0v5JezlL0YFoTySmfGpg9bR_AnJjQL7U-a_1SvjpDjsakjvqXmU-n6nJBiK5GV5PGZqM-178Mxz-_55KwOk4UiTWovw_r9dBBlPpldaj_OoH2Q0aCm4NRKuVdgld_YEOQRQKAD0e02vuJFLoQrnA-dn4_3KjWZSX9hO1nu5U_8MKo4RRCVpgbzfOyr19UVR5bun32Ed9xOpUZPtZEa45OWjX2r-OErBT_UuvIFe1b0hrlgY6DvAiJjKV-37Fp9LmWt7UkCuOFK3FDF_jozfOw_7EsbXuQ3CVNapwsv6dsVVSDnAl7CK8qOsptcqhuc-2wR5lY-v-aOrzL47VczL6UbvGFXjbtUwmTfIACXB147lUm6fWTs0FxAlzBr6xDXghz9NzemRzhLTLnumPJBxtQk36P1gRwLKPsYYgdpHoIO2JvupzriiKxyZpLjpW9_rp6G5DTiawHt17OGEaINx_Y-Kbwx_up3F40j2SxE6oszDvO70rgoAtyri39lNiE4015x9_L-pc25-fI12qFoirg40augyIzcHDIyr7zJH-SRqqEYi1ewRox_Qs6HdRU0vmc8tpXBTgCttXtO_MWvWVocpYisoVau6gLFb_op43L4mqCzEl0XhZRsjCMw-xSFcfgWDV_-NGSrSKSYNArUlmybb-TElu4vFAVkFX9qzex2E0jnnxX37DfGp=s200-no" data-holder-rendered="true" />
                        <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong className="d-block text-gray-dark">Meeting 1</strong>
                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                            tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                            fermentum massa justo sit amet risus.
                        </p>
                    </li>
                    <li className="media text-muted pt-3">
                        <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/qrz2VeUsUDTZ3JaT-UsdGuIbvOyJYKTz5h_P7ZIF_T70Qzgg1fy2eOeOB0v5JezlL0YFoTySmfGpg9bR_AnJjQL7U-a_1SvjpDjsakjvqXmU-n6nJBiK5GV5PGZqM-178Mxz-_55KwOk4UiTWovw_r9dBBlPpldaj_OoH2Q0aCm4NRKuVdgld_YEOQRQKAD0e02vuJFLoQrnA-dn4_3KjWZSX9hO1nu5U_8MKo4RRCVpgbzfOyr19UVR5bun32Ed9xOpUZPtZEa45OWjX2r-OErBT_UuvIFe1b0hrlgY6DvAiJjKV-37Fp9LmWt7UkCuOFK3FDF_jozfOw_7EsbXuQ3CVNapwsv6dsVVSDnAl7CK8qOsptcqhuc-2wR5lY-v-aOrzL47VczL6UbvGFXjbtUwmTfIACXB147lUm6fWTs0FxAlzBr6xDXghz9NzemRzhLTLnumPJBxtQk36P1gRwLKPsYYgdpHoIO2JvupzriiKxyZpLjpW9_rp6G5DTiawHt17OGEaINx_Y-Kbwx_up3F40j2SxE6oszDvO70rgoAtyri39lNiE4015x9_L-pc25-fI12qFoirg40augyIzcHDIyr7zJH-SRqqEYi1ewRox_Qs6HdRU0vmc8tpXBTgCttXtO_MWvWVocpYisoVau6gLFb_op43L4mqCzEl0XhZRsjCMw-xSFcfgWDV_-NGSrSKSYNArUlmybb-TElu4vFAVkFX9qzex2E0jnnxX37DfGp=s200-no" data-holder-rendered="true" />
                        <p className="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong className="d-block text-gray-dark">Meeting 2</strong>
                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                            tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                            fermentum massa justo sit amet risus.
                        </p>
                    </li>
                    <li className="media text-muted pt-3">
                        <img alt="32x32" className="mr-2 rounded" src="https://lh3.googleusercontent.com/qrz2VeUsUDTZ3JaT-UsdGuIbvOyJYKTz5h_P7ZIF_T70Qzgg1fy2eOeOB0v5JezlL0YFoTySmfGpg9bR_AnJjQL7U-a_1SvjpDjsakjvqXmU-n6nJBiK5GV5PGZqM-178Mxz-_55KwOk4UiTWovw_r9dBBlPpldaj_OoH2Q0aCm4NRKuVdgld_YEOQRQKAD0e02vuJFLoQrnA-dn4_3KjWZSX9hO1nu5U_8MKo4RRCVpgbzfOyr19UVR5bun32Ed9xOpUZPtZEa45OWjX2r-OErBT_UuvIFe1b0hrlgY6DvAiJjKV-37Fp9LmWt7UkCuOFK3FDF_jozfOw_7EsbXuQ3CVNapwsv6dsVVSDnAl7CK8qOsptcqhuc-2wR5lY-v-aOrzL47VczL6UbvGFXjbtUwmTfIACXB147lUm6fWTs0FxAlzBr6xDXghz9NzemRzhLTLnumPJBxtQk36P1gRwLKPsYYgdpHoIO2JvupzriiKxyZpLjpW9_rp6G5DTiawHt17OGEaINx_Y-Kbwx_up3F40j2SxE6oszDvO70rgoAtyri39lNiE4015x9_L-pc25-fI12qFoirg40augyIzcHDIyr7zJH-SRqqEYi1ewRox_Qs6HdRU0vmc8tpXBTgCttXtO_MWvWVocpYisoVau6gLFb_op43L4mqCzEl0XhZRsjCMw-xSFcfgWDV_-NGSrSKSYNArUlmybb-TElu4vFAVkFX9qzex2E0jnnxX37DfGp=s200-no" data-holder-rendered="true" />
                        <p className="media-body pb-3 mb-0 small lh-125">
                            <strong className="d-block text-gray-dark">Meeting 3</strong>
                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus,
                            tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                            fermentum massa justo sit amet risus.
                        </p>
                    </li>
                </ul>
            </div>
        )
    }
}

export default Events
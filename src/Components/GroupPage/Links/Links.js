import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Links extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render () {
        return (
            // LINK
            <div className="col-12 col-lg-4 mb-4 mb-lg-0 pl-0 pr-0 pr-lg-2">
                <h3 className="pb-2 mb-0">Links</h3>

                <div
                    className="link-container d-flex flex-wrap align-items-center justify-content-between">
                    <Link className="card text-decoration-none text-dark m-1" to="/customer-cataloge">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-3" alt="" />
                            <p className="card-text"><strong>Cataloge</strong></p>
                        </div>
                    </Link>
                    <Link className="card text-decoration-none text-dark m-1" to="/kaizen">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-3" alt="" />
                            <p className="card-text"><strong>Kaizen</strong></p>
                        </div>
                    </Link>
                    <Link className="card text-decoration-none text-dark m-1" to="/">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-3" alt="" />
                            <p className="card-text"><strong>Test</strong></p>
                        </div>
                    </Link>
                    <Link className="card text-decoration-none text-dark m-1" to="/">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-3" alt="" />
                            <p className="card-text"><strong>Test</strong></p>
                        </div>
                    </Link>
                    <Link className="card text-decoration-none text-dark m-1" to="/">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-3" alt="" />
                            <p className="card-text"><strong>Test</strong></p>
                        </div>
                    </Link>
                    <Link className="card text-decoration-none text-dark m-1" to="/">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-3" alt="" />
                            <p className="card-text"><strong>Test</strong></p>
                        </div>
                    </Link>
                    <a className="card text-decoration-none text-dark m-1" href="/">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-4" alt="" />
                            <p className="card-text"><strong>Test</strong></p>
                        </div>
                    </a>
                    <a className="card text-decoration-none text-dark m-1" href="/">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-4" alt="" />
                            <p className="card-text"><strong>Test</strong></p>
                        </div>
                    </a>
                    <a className="card text-decoration-none text-dark m-1" href="/">
                        <div className="card-body text-center">
                            <img src={require('../../../Assets/images/business-and-finance.svg')}
                                className="img-fluid mb-4" alt="" />
                            <p className="card-text"><strong>Test</strong></p>
                        </div>
                    </a>
                </div>
            </div>
        )
    }
}

export default Links
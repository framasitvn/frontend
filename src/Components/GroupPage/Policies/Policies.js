import React, { Component } from 'react';

class Policies extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render () {
        return (
            // <!-- POLICIES & PROCEDUCES -->
            <div className="col-12 col-sm-6 col-lg-4 mb-4 mb-lg-0">
                <h3 className="border-bottom border-gray pb-2 mb-3">Policies & Proceduces</h3>

                <ul className="nav nav-tabs" id="policies-procedues" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="home-tab" data-toggle="tab" href="#hr"
                            role="tab" aria-controls="home" aria-selected="true">HR</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="profile-tab" data-toggle="tab" href="#finance"
                            role="tab" aria-controls="profile" aria-selected="false">Finance</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="contact-tab" data-toggle="tab" href="#it"
                            role="tab" aria-controls="contact" aria-selected="false">IT</a>
                    </li>
                </ul>
                <div className="tab-content" id="policies-prcedues-content">
                    <div className="tab-pane fade show active" id="hr" role="tabpanel"
                        aria-labelledby="home-tab">
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item"><i className="fa fa-file-pdf-o mr-2"
                                    aria-hidden="true"></i>HR Document justo odio 1</li>
                            <li className="list-group-item"><i className="fa fa-file-pdf-o mr-2"
                                    aria-hidden="true"></i>HR Document facilisis in</li>
                            <li className="list-group-item"><i className="fa fa-file-word-o mr-2"
                                    aria-hidden="true"></i>HR Document risus</li>
                            <li className="list-group-item"><i className="fa fa-file-word-o mr-2"
                                    aria-hidden="true"></i>HR Document consectetur ac</li>
                            <li className="list-group-item"><i className="fa fa-file-powerpoint-o mr-2"
                                    aria-hidden="true"></i>HR Document at eros</li>
                        </ul>
                    </div>
                    <div className="tab-pane fade" id="finance" role="tabpanel"
                        aria-labelledby="profile-tab">
                        <ul className="list-group list-group-flush">
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item"><i className="fa fa-file-pdf-o mr-2"
                                        aria-hidden="true"></i>Finance justo odio 1</li>
                                <li className="list-group-item"><i className="fa fa-file-pdf-o mr-2"
                                        aria-hidden="true"></i>Finance facilisis in</li>
                                <li className="list-group-item"><i className="fa fa-file-word-o mr-2"
                                        aria-hidden="true"></i>Finance risus</li>
                                <li className="list-group-item"><i className="fa fa-file-word-o mr-2"
                                        aria-hidden="true"></i>Finance consectetur ac</li>
                                <li className="list-group-item"><i
                                        className="fa fa-file-powerpoint-o mr-2"
                                        aria-hidden="true"></i>Finance at eros</li>
                            </ul>
                        </ul>
                    </div>
                    <div className="tab-pane fade" id="it" role="tabpanel"
                        aria-labelledby="contact-tab">
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item"><i className="fa fa-file-pdf-o mr-2"
                                    aria-hidden="true"></i>IT Document justo odio 1</li>
                            <li className="list-group-item"><i className="fa fa-file-pdf-o mr-2"
                                    aria-hidden="true"></i>IT Document facilisis in</li>
                            <li className="list-group-item"><i className="fa fa-file-word-o mr-2"
                                    aria-hidden="true"></i>IT Document risus</li>
                            <li className="list-group-item"><i className="fa fa-file-word-o mr-2"
                                    aria-hidden="true"></i>IT Document consectetur ac</li>
                            <li className="list-group-item"><i className="fa fa-file-powerpoint-o mr-2"
                                    aria-hidden="true"></i>IT Document at eros</li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default Policies
import React, { Component } from 'react';
import axiosConfig from '../../../Store/axiosConfig.js';

class optionList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            multiple: "",
            result: []
        };
    }

    queryData = () => {
        axiosConfig.post(this.props.API)
            .then(res => {
                const result = res.data.result;
                let id =  this.props.component_ID;
                let multiple = this.props.MULTIPLE;
                this.setState({ id, multiple, result });

                if (this.state.multiple === "true") {
                    let selectMulti = document.querySelector(`#${this.state.id}`);

                    selectMulti.options[0].remove();
                    selectMulti.multiple = true
                    selectMulti.style.height = "240px";
                }
            })
            .catch(error => console.log(error));
    }

    componentDidMount() {
        this.queryData();
    } 

    render() {
        return (
            <select id={this.state.id} className="form-control" name={this.state.id}>
                {/* <option key="0" value="0"></option> */}
                { this.state.result.map(result => <option key={`${result.Name}-${result.Id}`} value={result.Id}>{result.Name}</option>) }
            </select>
        )
    }
}

export default optionList;
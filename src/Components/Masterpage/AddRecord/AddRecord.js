import React, { Component } from 'react';
import OPTRender from './optRender';
import axiosConfig from '../../../Store/axiosConfig.js';
import $ from 'jquery';

class AddRecord extends Component {
    constructor(props) {
        super(props);
        this.state = {
            record: {},
            addAPI: '/api/productseason/add'
        };
    }

    next_prev_StepHandler = () => {
        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;

        $(".next").click(function () {
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            console.log(current_fs.prop("id"))

            if ( current_fs.prop("id") === "Prices" ) {
                let priceRows = document.querySelectorAll('#prices_table tbody tr').length;
                
                if ( priceRows === 0 ) {

                    $("#prices_error").html(`<div class="alert alert-danger" role="alert">
                        Data must have at least one price
                    </div>`)

                } else if ( priceRows !== 0 && $("#prices_table input.required-field").length !== 0 ) {
                    
                    $("#prices_table input.required-field").each((i, el) => {
                        if ( el.value === "" ) {
                            $("#prices_error").html(`<div class="alert alert-warning" role="alert">
                                Please add data into the field and click (+)
                            </div>`)
    
                            el.classList.add("error");
                        }
                    })

                } else {

                    $("#prices_error").html("");
                    $("#components_error").html("");

                    //Add Class Active
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                
                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now) {
                            // for making fielset appear animation
                            opacity = 1 - now;
                
                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({
                                'opacity': opacity
                            });
                        },
                        duration: 600
                    });

                }
            } else if ( current_fs.prop("id") === "Components" ) {
                let componentRows = document.querySelectorAll('#components_table tbody tr').length;

                if ( componentRows === 0 ) {

                    $("#components_error").html(`<div class="alert alert-danger" role="alert">
                        Data must have at least one price
                    </div>`)

                } else if ( componentRows !== 0 && $("#components_table input.required-field").length !== 0 ) {
                    
                    $("#components_table input.required-field").each((i, el) => {
                        if ( el.value === "" ) {
                            $("#prices_error").html(`<div class="alert alert-warning" role="alert">
                                Please add data into the field and click (+)
                            </div>`)
    
                            el.classList.add("error");
                        }
                    })

                } else {

                    $("#components_error").html("");

                    //Add Class Active
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                
                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function (now) {
                            // for making fielset appear animation
                            opacity = 1 - now;
                
                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({
                                'opacity': opacity
                            });
                        },
                        duration: 600
                    });

                }
            } else {
                //Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            
                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;
            
                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 600
                });
            }
        });

        $(".previous").click(function () {

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
        
            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
            //show the previous fieldset
            previous_fs.show();
        
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;
        
                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({
                        'opacity': opacity
                    });
                },
                duration: 600
            });
        });
    }

    closeModalHandler = () => {
        var form_fieldset = document.querySelectorAll("fieldset");
        var inputField = document.querySelectorAll("#msform .form-control");

        form_fieldset.forEach(el => {
            el.style = ""
        })
        
        inputField.forEach(el => {
            if (el.type === "select-one" || el.type === "select-multiple" ) {
                el.selectedIndex = 0
            } else {
                el.val = ""
            }
        });

        document.querySelector("#step2").classList = ""
        document.querySelector("#step3").classList = ""
        document.querySelector("#step4").classList = ""
        document.querySelector("#step5").classList = ""
    }

    ui_validationFieldHandler = () => {
        
    }

    server_validationFieldHandler = (status) => {
        console.log("server_validationFieldHandler")
    }

    addPriceHandler = () => {
        $(document).ready(function(){
            var count_price = 0;
            var title = document.querySelector(".table-price-title h3");

            $('[data-toggle="tooltip"]').tooltip();
            var actions = `
                <span class="add" title="Add" data-toggle="tooltip"><i class="fa fa-plus-circle mr-2" aria-hidden="true"></i></span>
                <span class="edit" title="Edit" data-toggle="tooltip"><i class="fas fa-pencil-alt text-warning mr-2"></i></span>
                <span class="delete" title="Delete" data-toggle="tooltip"><i class="fa fa-trash mr-2 text-danger" aria-hidden="true"></i></span>
            `;

            // Append table with add row form on add new button click
            $(".add-new-price").click(function(){
                count_price++;
                title.innerHTML = `<strong>Price</strong> (${count_price})`

                $(this).attr("disabled", "disabled");
                var index = $("#prices_table tbody tr:last-child").index();
                var row = `<tr>` +
                    `<td><input type="number" class="form-control price-table-field required-field" name="price" id="price-${index}" placeholder="0" min="0"></td>` +
                    `<td><input type="text" class="form-control price-table-field" name="description" id="description-${index}" placeholder="Regular Item Price (USD)"></td>` +
                    `<td>` + actions + `</td>` +
                '</tr>';
                $("#prices_table").append(row);		
                $("#prices_table tbody tr").eq(index + 1).find(".edit").toggle();
                // $('[data-toggle="tooltip"]').tooltip();
            });

            // Add row on add button click
            $(document).on("click", ".add", function(){
                var empty = false;
                var input = $(this).parents("tr").find('input.price-table-field');

                input.each(function(){
                    if(!$(this).val() && $(this).hasClass("required-field")){
                        $(this).addClass("error");
                        empty = true;
                    } else{
                        $(this).removeClass("error");
                    }
                });
                $(this).parents("tr").find(".error").first().focus();
                if(!empty){
                    input.each(function(){
                        if(!$(this).val() && !$(this).hasClass("required-field")) {
                            $(this).parent("td").html("N/A");
                        } else {
                            $(this).parent("td").html($(this).val()).addClass("has-val");
                        }
                    });			
                    $(this).parents("tr").find(".add, .edit").toggle();
                    $(".add-new-price").removeAttr("disabled");
                }		
            });
            
            // Edit row on edit button click
            $(document).on("click", ".edit", function(){
                var i = 0;		
                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    i++;
                    if (i === 1) {
                        $(this).html('<input type="number" class="form-control price-table-field required-field" value="' + $(this).text() + '" placeholder="0" min="0">');
                    } else {
                        $(this).html('<input type="text" class="form-control price-table-field" value="' + $(this).text() + '" placeholder="Regular Item Price (USD)">');
                    }
                });		
                $(this).parents("tr").find(".add, .edit").toggle();
                $(".add-new-price").attr("disabled", "disabled");
            });

            // Delete row on delete button click
            $(document).on("click", ".delete", function(){
                count_price--;
                title.innerHTML = `<strong>Price</strong> (${count_price})`

                $(this).parents("tr").remove();
                $(".add-new-price").removeAttr("disabled");
            });
        });
    }

    addComponentHandler = () => {
        $(document).ready(function(){
            var count_component = 0;
            var title = document.querySelector(".table-component-title h3");

            var selectComponents = `
                <select class="form-control component-table-field">
                    <option value="0">All</option>
            `;
            
            axiosConfig.post('/api/component/all')
                .then(res => {
                    const result = res.data.result;
                    
                    result.forEach(el => {
                        selectComponents += `
                            <option key="${el.Name}-${el.Id}" value="${el.Id}">"${el.Name}"</option>
                        `
                    });

                    selectComponents += `</select>`;
                })
                .catch(error => console.log(error));

            var actions = `
                <span class="add-component" title="Add" data-toggle="tooltip"><i class="fa fa-plus-circle mr-2" aria-hidden="true"></i></span>
                <span class="edit-component" title="Edit" data-toggle="tooltip"><i class="fas fa-pencil-alt text-warning mr-2"></i></span>
                <span class="delete-component" title="Delete" data-toggle="tooltip"><i class="fa fa-trash mr-2 text-danger" aria-hidden="true"></i></span>
            `;

            // Append table with add row form on add new button click
            $(".add-new-component").click(function(){
                count_component++;
                title.innerHTML = `<strong>Components</strong> (${count_component})`

                $(this).attr("disabled", "disabled");
                var index = $("#components_table tbody tr:last-child").index();
                var row = `<tr>` +
                    `<td>` + selectComponents + `</td>` +
                    `<td><input type="number" class="form-control component-table-field required-field" name="Shotweight" placeholder="0" min="0"></td>` +
                    `<td><input type="text" class="form-control component-table-field required-field" name="Main Material" placeholder="Pebax 63R53"></td>` +
                    `<td><input type="text" class="form-control component-table-field required-field" name="Alt. Mat. Trans" placeholder="30% Virgin + 70% framapur 95A"></td>` +
                    `<td><input type="text" class="form-control component-table-field required-field" name="Alt. Mat. Black" placeholder="30% Virgin + 70% framapur 95A"></td>` +
                    `<td>` + actions + `</td>`+
                '</tr>';

                $("#components_table").append(row);		
                $("#components_table tbody tr").eq(index + 1).find(".edit-component").toggle();
            });

            // Add row on add button click
            $(document).on("click", ".add-component", function(){
                var empty = false;
                var input = $(this).parents("tr").find('.component-table-field');

                input.each(function(){
                    if(!$(this).val()){
                        $(this).addClass("error");
                        empty = true;
                    } else{
                        $(this).removeClass("error");
                    }
                });
                $(this).parents("tr").find(".error").first().focus();
                var j = 0;
                if(!empty){
                    input.each(function(){
                        j++;
                        if (j === 1) {
                            $(this).parent("td").attr('data-selected-component', $(this).val()).html($(this).find('option:selected').text())
                        } else {
                            $(this).parent("td").html($(this).val())
                        }
                        
                    });			
                    $(this).parents("tr").find(".add-component, .edit-component").toggle();
                    $(".add-new-price").removeAttr("disabled");
                }		
            });

            // Edit row on edit button click
            $(document).on("click", ".edit-component", function(){
                var i = 0;		
                $(this).parents("tr").find("td:not(:last-child)").each(function(){
                    i++;
                    switch (i) {
                        case 1:
                            $(this).html(selectComponents);
                            break;
                        case 2:
                            $(this).html(`<input type="number" class="form-control component-table-field required-field" name="Shotweight" placeholder="0" min="0" value="${$(this).text()}">`);
                            break;
                        case 3:
                            $(this).html(`<input type="text" class="form-control component-table-field required-field" name="Main Material" placeholder="Pebax 63R53" value="${$(this).text()}">`);
                            break;
                        case 4:
                            $(this).html(`<input type="text" class="form-control component-table-field required-field" name="Alt. Mat. Trans" placeholder="30% Virgin + 70% framapur 95A" value="${$(this).text()}">`);
                            break;
                        case 5:
                            $(this).html(`<input type="text" class="form-control component-table-field required-field" name="Alt. Mat. Black" placeholder="30% Virgin + 70% framapur 95A" value="${$(this).text()}">`);
                            break;
                        default:
                            break;
                    }
                });		
                $(this).parents("tr").find(".add-component, .edit-component").toggle();
                $(".add-new-component").attr("disabled", "disabled");
            });

            // Delete row on delete button click
            $(document).on("click", ".delete-component", function(){
                count_component--;
                title.innerHTML = `<strong>Components</strong> (${count_component})`

                $(this).parents("tr").remove();
                $(".add-new-component").removeAttr("disabled");
            });
        });
    }

    componentDidMount() {
        this.next_prev_StepHandler();
        this.addPriceHandler();
        this.addComponentHandler();
    }

    render () {
        return (
            <div className="modal fade" id="addRecord" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle mr-2" aria-hidden="true">
                {/* <!-- Modal - Add Record --> */}
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header border-bottom-0 pb-0">
                            <button type="button" className="btn-close-form close" data-dismiss="modal" aria-label="Close" onClick={this.closeModalHandler}>
                                <i className="fa fa-times text-danger mr-2" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div className="modal-body">
                            {/* <!-- MultiStep Form --> */}
                            <div className="container-fluid" id="grad1">
                                <div className="row justify-content-center mt-0">
                                    <div className="col text-center p-0 mt-3 mb-2">
                                        <div className="card px-0 pb-0 mb-3">
                                            <h2 className="modal-heading"><strong>Add New Record To Database</strong></h2>
                                            <p>Fill all form field to go to next step</p>
                                            <div className="row">
                                                <div className="col-md-12 mx-0">
                                                    <form id="msform">
                                                        {/* <!-- progressbar --> */}
                                                        <ul id="progressbar" className="p-0">
                                                            <li className="active" id="step1"><strong>Main</strong></li>
                                                            <li id="step2"><strong>Price</strong></li>
                                                            <li id="step3"><strong>Components</strong></li>
                                                            <li id="step4"><strong>Shoes Factory</strong></li>
                                                            <li id="step5"><strong>Images</strong></li>
                                                        </ul>
                                                        {/* <!-- fieldsets --> */}

                                                        {/* STEP 1: Main */}
                                                        <fieldset id="Main">
                                                            <div className="form-card">
                                                                <div className="form-row">
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="factory_id">Factory</label>
                                                                            <OPTRender component_ID="factory_id" API="/api/factory/all" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="project_id">Item</label>
                                                                            <OPTRender component_ID="project_id" API="/api/project/all" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="country_id">Domestic/Export</label>
                                                                            <OPTRender component_ID="country_id" API="/api/country/all" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="form-row">
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="season_start">Season Starting</label>
                                                                            <OPTRender component_ID="season_start" API="/api/season/all" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="season_for">Production for season XY</label>
                                                                            <OPTRender component_ID="season_for" API="/api/season/all" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="lead_times">Lead Time (days)</label>
                                                                            <input type="text" className="form-control" id="lead_times" placeholder="" />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="form-row">
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="seasonal_forecast">Seasonal Forecast</label>
                                                                            <input type="number" className="form-control" id="seasonal_forecast" placeholder="12,310.01" min="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="max_month_ppic">Max. Monht PPIC</label>
                                                                            <input type="number" className="form-control" id="max_month_ppic" placeholder="17,879.88" min="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="shoe_factory_id">Shoes Factory</label>
                                                                            <OPTRender component_ID="shoe_factory_id" API="/api/shoefactory/all" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="colours_amount">Amt. of Colours</label>
                                                                            <input type="number" className="form-control" id="colours_amount" placeholder="12,310.01" min="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="sizes_amount">Amt. of Sizes</label>
                                                                            <input type="number" className="form-control" id="sizes_amount" placeholder="17,879.88" min="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="mold_amount">Amt. of Molds</label>
                                                                            <input type="number" className="form-control" id="mold_amount" placeholder="12,310.01" min="0" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="form-row">
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="part_weight_runner">Partweight Excl. Runner [g]</label>
                                                                            <input type="number" className="form-control" id="part_weight_runner" placeholder="" min="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="cbd_total_shot_weight">CBD Total Shotweight [g]</label>
                                                                            <input type="number" className="form-control" id="cbd_total_shot_weight" placeholder="15,987.32" min="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="total_shot_weight">Total Shotweight [g]</label>
                                                                            <input type="number" className="form-control" id="total_shot_weight" placeholder="0" min="0" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div className="form-row">
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="Nr_steps">Nr. of Steps</label>
                                                                            <input type="text" className="form-control" id="Nr_steps" placeholder="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="form-group">
                                                                            <label htmlFor="Nr_steps">Is Decoration or Not?</label>
                                                                            <div className="custom-control custom-checkbox my-1 mr-sm-2">
                                                                                <input type="checkbox" className="form-control custom-control-input" id="is_decoration" />
                                                                                <label className="custom-control-label" htmlFor="is_decoration">Decoration</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col"></div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <input type="button" name="close"
                                                                className="btn-close-form action-button bg-danger"
                                                                data-dismiss="modal" value="Cancel" onClick={this.closeModalHandler} />
                                                            <input type="button" name="next" className="next action-button"
                                                                value="Next Step" />
                                                        </fieldset>

                                                        {/* STEP 2: Prices */}
                                                        <fieldset id="Prices">
                                                            <div className="form-card">
                                                                <div id="prices_error"></div>

                                                                <div className="table-wrapper">
                                                                    <div className="table-price-title">
                                                                        <div className="row">
                                                                            <div className="col-sm-8"><h3><strong>Prices</strong> (0)</h3></div>
                                                                            <div className="col-sm-4 text-right">
                                                                                <span className="btn btn-success add-new-price"><i className="fa fa-plus"></i> Add New</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <table id="prices_table" className="table table-bordered">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Price</th>
                                                                                <th>Description</th>
                                                                                <th>Actions</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                                
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <input type="button" name="previous"
                                                                className="previous action-button-previous" value="Previous" />
                                                            <input type="button" name="next" className="next action-button"
                                                                value="Next Step" />
                                                        </fieldset>

                                                        {/* STEP 3: Components */}
                                                        <fieldset id="Components">
                                                            <div className="form-card">
                                                                <div id="components_error"></div>

                                                                <div className="table-wrapper">
                                                                    <div className="table-component-title">
                                                                        <div className="row">
                                                                            <div className="col-sm-8"><h3><strong>Components</strong> (0)</h3></div>
                                                                            <div className="col-sm-4 text-right">
                                                                                <span className="btn btn-success add-new-component"><i className="fa fa-plus"></i> Add New</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div className="table-container">
                                                                        <table id="components_table" className="table table-bordered">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Components</th>
                                                                                    <th>Shotweight [g]</th>
                                                                                    <th>Main Material</th>
                                                                                    <th>Alt. Mat. Trans</th>
                                                                                    <th>Alt. Mat. Black</th>
                                                                                    <th>Actions</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <input type="button" name="previous"
                                                                className="previous action-button-previous" value="Previous" />
                                                            <input type="button" name="make_payment"
                                                                className="next action-button" value="Next Step" />
                                                        </fieldset>

                                                        {/* STEP 4: Shoes Factories */}
                                                        <fieldset id="Shoes_Factories">
                                                            <div className="form-card">
                                                                <div className="form-group">
                                                                    <h3 htmlFor="shoe_factories"><strong>Shoes Factory</strong></h3>
                                                                    <OPTRender component_ID="shoe_factories" API="/api/shoefactory/all" MULTIPLE="true"/>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <input type="button" name="previous"
                                                                className="previous action-button-previous" value="Previous" />
                                                            <input type="button" name="next" className="next action-button"
                                                                value="Next Step" />
                                                        </fieldset>
                                                        
                                                        {/* STEP 5: Image */}
                                                        <fieldset id="Image">
                                                            <div className="form-card">
                                                                <h3>Upload Image</h3>
                                                                
                                                                <div id="image_error"></div>

                                                                <div className="form-group mt-3">
                                                                    <input type="file" className="form-control form-control-file border-0" id="upload_img" />
                                                                </div>
                                                                <small id="fileHelp" className="form-text text-muted">Only accept images file input with format: "jpg, png, bmp, gif, tif…"</small>
                                                            </div>
                                                            <br />
                                                            <input type="button" name="previous"
                                                                className="previous action-button-previous" value="Previous" />
                                                            <input type="button" id="btn-submit-record" name="submit"
                                                                className="submit action-button btn btn-danger"
                                                                value="Submit" />
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddRecord;
import React, { Component } from 'react';
import axiosConfig from '../../../Store/axiosConfig.js';
import APIController from '../../../Store/APIController';
import $ from 'jquery';

class Datatable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            factoryID: [],
            productSeason: [],
            user: 'dat.vo'
        };
    }
    
    addRecordHandler = () => {
        const btn_submit_record = $("#btn-submit-record");
        const uploadImage = document.querySelector("#upload_img");
        var images = [];

        uploadImage.addEventListener('change', (e) => {
            var file = e.target.files[0];
            let reader = new FileReader();
            
            reader.readAsDataURL(file);
            reader.onload = (e) => { 
                let file_obj = {
                    "document_id": 0,
                    "file_extend": file.type,
                    "file_name": file.name,
                    "path": e.target.result
                }
                images.push(file_obj);
            }
        });

        btn_submit_record.click(() => {
            const btnClose = $(".btn-close-form");
            const inputShoesFactories = $('#shoe_factories option:selected');
            
            // Convert Prices Table to OBJ
            var prices = $('#prices_table tr:has(td)').map(function(i, v) {
                var $td =  $('td', this);
                var isMainPrice = false;
                ++i;
                if (i === 1) {
                    isMainPrice = true
                }
                
                return {
                    description: $td.eq(1).text(),
                    index: i,
                    is_main_price: isMainPrice,
                    value: parseInt($td.eq(0).text()),
                }
            }).get();

            // Convert Components Table to OBJ
            var components = $('#components_table tr:has(td)').map(function(i, v) {
                var $td =  $('td', this);
                    return {
                        index: ++i,
                        component_id: parseInt($td.eq(0).attr('data-selected-component')),
                        shot_weight: parseInt($td.eq(1).text()),
                        main_material: $td.eq(2).text(),
                        alt_mat_black: $td.eq(3).text(),
                        alt_mat_trans: $td.eq(4).text(),
                    }
            }).get();

            // Get Shoes Factories Selected options
            var shoes_factories = [];
            inputShoesFactories.each(function() {
                let selected = { shoe_factory_id: $(this).val() }
                shoes_factories.push(selected)
            });
            
            // let file_name = $("#upload_img").val().split(/(\\|\/)/g).pop().split('.')[0];
            // let file_extend = $("#upload_img").val().split(/(\\|\/)/g).pop().split(".").pop();

            let newRecord = {
                "cbd_total_shot_weight": 0,
                "cleat_material": "string",
                "colours_amount": parseInt($("#colours_amount").val()),
                "components": components,
                "country_id": parseInt($("#country_id").val()),
                "factory_id": parseInt($("#factory_id").val()),
                "images": images,
                "is_decoration": $("#is_decoration").is(":checked"),
                "lead_times": $("#lead_times").val(),
                "max_month_ppic": $("#max_month_ppic").val(),
                "mold_amount": $("#mold_amount").val(),
                "part_weight_runner": $("#part_weight_runner").val(),
                "prices": prices,
                "project_id": parseInt($("#project_id").val()),
                "season_for": parseInt($("#season_for").val()),
                "season_start": parseInt($("#season_start").val()),
                "seasonal_forecast": parseInt($("#seasonal_forecast").val()),
                "shoe_factories": shoes_factories,
                "shoe_factory_id": parseInt($("#shoe_factory_id").val()),
                "sizes_amount": parseInt($("#sizes_amount").val())
            }
            
            axiosConfig({
                method: 'post',
                url: this.state.addAPI,
                data: {
                    meta_data: newRecord,
                    username: this.state.user
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => {
                    console.log(res);
                    btnClose.click();
                    this.componentDidMount();
                    $('.toast').toast('show');
                }).catch(err => {
                    console.log(err)
                })
        })
    }

    // QUERY DATA
    queryFactoryID = () => {
        APIController.API.queryFactory()
            .then(res => {
                const result = res.data.result;
                let factoryID = [];
                var resultProccessed = 0;

                result.forEach(el => {
                    factoryID.push(el.Id);
                    resultProccessed++;
                    if (resultProccessed === result.length) {
                        this.setState({ factoryID }, () => {
                            this.queryProductSeasonbyFactory(this.state.factoryID);
                        });
                    }
                });                
            })
            .catch(error => console.log(error));
    }

    queryProductSeasonbyFactory = (factoryID) => {
        let productSeason = [];
        var factoryProcessed = 0;
        
        factoryID.forEach(ID => {
            APIController.API.queryProductSeasonByFactory(ID)
                .then(res => {
                    res.data.result.forEach(obj => {
                        let item = {
                            "project_ID": obj.ProjectId,
                            "name": obj.ProductGroupName,
                            "category": obj.CategoryName,
                            "location": obj.FactoryName,
                            "customer": obj.CustomerName,
                            "tooling_no": obj.ToolingNo,
                            "created_by": obj.CreatedBy + ` [${obj.CreatedDate}]`
                        }

                        productSeason.push(item)
                    });
                    factoryProcessed++;
                    if (factoryProcessed === factoryID.length) {
                        this.setState({
                            productSeason
                        }, () => {
                            this.recordTableInit(this.state.productSeason)
                        });
                    }
                })
                .catch(error => console.log(error));
        });
    }

    recordTableInit = (data) => {
      require( 'datatables.net-responsive-bs4' );

      $('#product_season_datatable').DataTable({
        "destroy": true,
        columns: [
            { data: 'project_ID' },
            { data: 'name' },
            { data: 'category' },
            { data: 'location' },
            { data: 'customer' },
            { data: 'tooling_no' },
            { data: 'created_by' }
        ],
        "data": data,
        "scrollY": "45vh",
        "scrollCollapse": true,
        "paging": true,
        responsive: true,
        scroller: {
            loadingIndicator: true
        },
        "order": [[ 0, "asc" ]]
      });
    }

    componentDidMount() {
        this.queryFactoryID();
        this.addRecordHandler();
    }
    
    render () {
        return (
            <div>
                <div className="page-datatable mt-3 mb-3">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col">
                              <span name="" id="" className="btn btn-success btn-add-record mb-3"
                                  data-toggle="modal" data-target="#addRecord" data-backdrop="static"
                                  data-keyboard="false" role="button"><i className="fa fa-plus mr-2"
                                      aria-hidden="true"></i>Add Record</span>

                                <table id="product_season_datatable" className="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Project ID</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Location</th>
                                            <th>Customer</th>
                                            <th>Tooling No</th>
                                            <th>Created by</th>
                                        </tr>
                                    </thead>

                                    <tbody></tbody>
                                </table>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }   
}

export default Datatable;
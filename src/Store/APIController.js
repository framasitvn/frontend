import axios from 'axios';

const apiClient = axios.create({
    baseURL: 'http://192.168.9.54:8088/'
});

// instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

export default {
    API: {
        queryComponents() {
            return apiClient.post('/api/component/all');
        },
        queryFactory() {
            return apiClient.post('/api/factory/all');
        },
        queryProductSeasonByFactory(factoryID) {
            return apiClient.post(`/api/productseason/by_factory/${factoryID}`);
        },
        queryContactInfo() {
            return apiClient.post('/api/contactinfo/all');
        },
        querySeason() {
            return apiClient.post('/api/season/all');
        },
        addContactInfo () {
            return apiClient.post('/api/contactinfo/add');
        },
        addProductSeasson() {
            return apiClient.post('/api/productseason/add');
        },
        addSeason() {
            return apiClient.post('/api/season/add');
        },
        modifySeason() {
            return apiClient.post('/api/season/modify');
        }
    }
};
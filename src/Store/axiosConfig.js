import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://192.168.9.54:8088/'
});

// instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';

export default instance;
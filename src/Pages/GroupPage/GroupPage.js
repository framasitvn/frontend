import React, { Component } from 'react';
import PageHeader from '../../Components/PageHeader/PageHeader';
import PageFooter from '../../Components/PageFooter/PageFooter';
import PageNews from '../../Components/GroupPage/News/News';
import PageLocations from '../../Components/GroupPage/Locations/Locations';
import PagePolicies from '../../Components/GroupPage/Policies/Policies';
import PageTemplate from '../../Components/GroupPage/Templates/Templates';
import PageEvents from '../../Components/GroupPage/Events/Events';
import PageLinks from '../../Components/GroupPage/Links/Links';
import PageColleagues from '../../Components/GroupPage/Colleagues/Colleagues';
import PageCalendar from '../../Components/GroupPage/Calendar/Calendar';

class GroupPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchToggle: false
    };
  }
  
  render () {
    return (
      <div>
        <PageHeader
          pageNav="false"
          headline="A Place for Entrepreneurs to Share and Discover New Stories"
          backgroundImage="https://lh3.googleusercontent.com/dhnGn46Z_ST887yLbH1eA6Pe75I5sRGeqg5mRFjr0KIK9caiSwADHPu7P6us7cLRjtqb2KGBPYBeaQ9U0AqnMjnpAPiC8xc4dMc8L_VWBcj9LO2vfGgJhjlYHSDNEFxDAOOiikEju-qXpWSwSOkU9n5RORdB8pLowwOVkB4VA-0OllUPTFmMBFzTxVM1dpjXO9T_4nu5Q4ZIw308Ow8-besc97gT8FWEMWF4z-8aX3byuEH4cO4hG76TcA0Rj1i2WS4wNSgpk6iqndbrEZYU305pA3vVp0ptVkCDOgd_6wT7GD1XbBbkRvZD4vYdfcb_xrneVaJPKPaYVxgNsqfoJbQoGYIUIkNNmuDL_OI7wxWRcG9GwjmYK0anHiW5fupD1POoKQ45P5Dj5smC-Zx51BITo_aSyFj-yb9uvDiNMffJBig4aeIF3oWsClPhBXj-C_EUGGzYEPZWi3Q-qzlMztLeed28P2ubLVa-DNvvKbV31FEinY_hNx-xsbTlkZY6w2SlAqscaxrfkz9KDWjXGvMYcz7HRwYsgWMi1cq9NIKyyad0qDTSvs_QTtsFZSVaH9RGQYRMmCPbSwqiu_zzj0NZdarUcBQUv3O-uTKj6MytaBthRP3VoHMN4m3Cx-DSv8R_JxI8AVV7C30q8MbHa_roeN8yBXJLK4bOBZrtWL0FXy4u7Acg7Y1649fl5110JBPY61T3ZpF6oAqGU3bZd5rBkWaTMpRqcHKumi4QjF_X1tSp=w1600-h1065-no"/>
        
        <div className="page-content">
          <div className="container-fluid pt-4 pb-4">
            <div className="row">
              <div className="col">
                <PageNews />

                {/* <!-- PAGE DOCUMENTS & EVENTS --> */}
                <div className="page-docs-events mt-3 mt-lg-5">
                  <div className="row">
                    <PagePolicies />
                    <PageTemplate />
                    <PageEvents />
                  </div>
                </div>

                {/* <!-- LINK & COLLEGE --> */}
                <div className="page-link-college mt-5">
                  <div className="container-fluid">
                    <div className="row">
                      <PageLinks />
                      <PageColleagues />
                    </div>
                  </div>
                </div>

                <PageCalendar />

                <PageLocations />
              </div>
            </div>
          </div>
        </div>
  
        <PageFooter />
      </div>
    )
  }
}

export default GroupPage;
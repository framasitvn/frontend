import React, { Component } from 'react';
import PageHeader from '../../Components/PageHeader/PageHeader';
import PageFooter from '../../Components/PageFooter/PageFooter';
import Season from '../../Components/List_Data/Season/Season';
import SeasonPopUp from '../../Components/List_Data/Season/Season_PopUp';

class MasterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render () {
    return ( 
      <div>
        <PageHeader
          pageNav="true"
          headline="Season Table"
          backgroundImage="https://lh3.googleusercontent.com/kOpw3Wq7vpUnQX7u5eagIyDmWbyHIJURClPYmDT9EMnkp_uT88yj0SEzjMlHzHZ8aZG9JmjtBlTNNQfj17nPyFhVvWM2ssEBguUta8C5t78x1QwrvSUvQVzn8Z1cu3vLpu4Pfz9YIidzGxp6sXU-ClLkYdlUcLPS9rRmvT38T6BAWjDaKF5bkx7GL46KEMvpi7wvgbR2ad47op9kMl6c-J7nxNlWmUP60JSk7rT0gyuimMX89ij4oIIcHQ47JUWOQqMLtqTw3XMFOh1TVSnDdT6F178fIE9ScWbdjcittYsOCYE499VWd704ANAtuKMiuHydkF4D8ps-N09DtkkeYsUMJpHEcRU7HP8cB2YcRDJ6ukxq30S62gwF_lkmAb7HQvbauCLHcG0j4ryz_6nweIVxgJECv-lqIObAU82BPJgxoqedxeEX8UEoZq2SFCwhDcBYjM4zPFiWXBQo6_yLw5jXu426Y2fe6fKB3UAVW03Ilp5M-5F-cthRXCPEUamipaf7rwd-U1OWLEphTwI1JE0qOy21JmrS2biwR2CqFhfp8Wy3MS1_rX1PI9k3rbATu3miWtYMle6eeF8lsYaLlgf2oBYYstsCOXW_Ve_0zrgCdadwKFNegcwzu_kcYJg9F4pa8VcvfAp-3R1KHBlcqy7l37_KDkeIYmSbBrg_O55E68aOvShT8r9LZolz9Er2Q1eLRtwe_5fTO6srUS_1OR80gEaqu4w2-vjEAAwUTPX7C4hg=w1371-h912-no"/>
        
        <div className="page-content">
          <div className="container-fluid pt-4 pb-4">
            <div className="row">
              <div className="col p-0">
                <Season />
              </div>
            </div>
          </div>
        </div>
  
        <PageFooter />

        <SeasonPopUp />
      </div>
    );
  }
}


export default MasterPage;

import React from 'react';

import Headline from '../../Components/CustomerCataloge/Headline/Headline';
import Footer from '../../Components/CustomerCataloge/Footer/Footer';
import ReportSettings from '../../Components/CustomerCataloge/Report_settings/Report_settings';
import Report from '../../Components/CustomerCataloge/Report/Report';

const CustomerCataloge = () => {
  return (
        <div>
          <Headline />

          <ReportSettings />

          <Report />

          <Footer />
        </div>
  );
}


export default CustomerCataloge;

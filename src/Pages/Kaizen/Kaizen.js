import React from 'react';
import PageHeader from '../../Components/PageHeader/PageHeader';
import PageFooter from '../../Components/PageFooter/PageFooter';

const Kaizen = () => {
  return (
    <div>
      <PageHeader
        pageNav="true"
        nav="{}"
        headline="Kaizen System"
        backgroundImage="https://lh3.googleusercontent.com/kOpw3Wq7vpUnQX7u5eagIyDmWbyHIJURClPYmDT9EMnkp_uT88yj0SEzjMlHzHZ8aZG9JmjtBlTNNQfj17nPyFhVvWM2ssEBguUta8C5t78x1QwrvSUvQVzn8Z1cu3vLpu4Pfz9YIidzGxp6sXU-ClLkYdlUcLPS9rRmvT38T6BAWjDaKF5bkx7GL46KEMvpi7wvgbR2ad47op9kMl6c-J7nxNlWmUP60JSk7rT0gyuimMX89ij4oIIcHQ47JUWOQqMLtqTw3XMFOh1TVSnDdT6F178fIE9ScWbdjcittYsOCYE499VWd704ANAtuKMiuHydkF4D8ps-N09DtkkeYsUMJpHEcRU7HP8cB2YcRDJ6ukxq30S62gwF_lkmAb7HQvbauCLHcG0j4ryz_6nweIVxgJECv-lqIObAU82BPJgxoqedxeEX8UEoZq2SFCwhDcBYjM4zPFiWXBQo6_yLw5jXu426Y2fe6fKB3UAVW03Ilp5M-5F-cthRXCPEUamipaf7rwd-U1OWLEphTwI1JE0qOy21JmrS2biwR2CqFhfp8Wy3MS1_rX1PI9k3rbATu3miWtYMle6eeF8lsYaLlgf2oBYYstsCOXW_Ve_0zrgCdadwKFNegcwzu_kcYJg9F4pa8VcvfAp-3R1KHBlcqy7l37_KDkeIYmSbBrg_O55E68aOvShT8r9LZolz9Er2Q1eLRtwe_5fTO6srUS_1OR80gEaqu4w2-vjEAAwUTPX7C4hg=w1371-h912-no"/>
      
      <div className="page-content">
        <div className="container-fluid pt-4 pb-4">
          <div className="row">
            <div className="col">
                <img src="https://lh3.googleusercontent.com/Y2fWGWd18CUS8cmDu_t_dJOX2nqKoMGn2ADscFVqGf0UI9eJd950IH7FW9SgsnkJSAyt7-h_Kl0TYIR26zvAO4BwxTZoD_XZBDOIas2QWkxAqIKULbwpA7LCeobTZcm8xBYzSnYSsSdI3cbpz2TU-B-_G-Nkdf4W5pm4OfGPQMKV5I7CpJrq-ni_6Z6vRPChjL6iKqZVYI7r9FfoxR-DbPR6VsKE6PsIHUms0AD9qtGEZExTPiK9GQ-5RHGwCyTl7C3rs_MqFmH0Mvj5r78iOzioBJm2Nrk6QMLD8MZ9uEXThlBwAU0KMLwxFJIAngjA86BTXvVK0e8_uxk7Gi6hiqitkE7WNCKydmCadvjJ17LzjT8tqsDBPr-6yyb0AX8y6DUD9HXKF8rxNmSr7-MZLywgDjmXFr9x9WZURlhW1g5qUJyXri-oJ-PsejOwJoSDssp4_MVkVuwwU_5Q7_Too6-fjW3jizjZMTzFJH1KrWPvYQeJVx7s3HY8sDaBEDdgKeVC8krNxkLEmPCNfmnI_zxSai3LLBeKxhL5lFYCo_xuoSX-3AWMqnGffn64ihyulk8JKeLYnDVYeSXPbNko54CflrsRiYnBa1nFHzxZskGSbHv7CX-i3pzMN2piNifncB4K9CLYFNqVrgMJGSQGG6Vq4kzuoCNEu9qTOY7uAHRZTMbtnmBsnKEJo8AfIHpoaJp-5BRV3vBg3Mrfi1pbbwG3c6_bJ1Hr39B6WjgUysnWe-5O=w1834-h1040-no" 
                    className="img-fluid" 
                    alt="kaizen-system" />
            </div>
          </div>
        </div>
      </div>

      <PageFooter />
    </div>
  );
}


export default Kaizen;
